<?php

require_once("../lib/include.php");

$gCms = CmsApp::get_instance();
$db = $gCms->GetDb();

// Get counter
$q = "SELECT id FROM " . cms_db_prefix() . 'module_products_history_seq';
$number = $db->GetOne($q);
$number = isset($number) ? $number : 0;

$p = "SELECT id,product_id FROM " . cms_db_prefix() . 'module_products_history ORDER BY id DESC LIMIT 0,1';
$pnumber = $db->GetRow($p);

$pd = "SELECT id,sku,owner,modified_date FROM " . cms_db_prefix() . "module_products WHERE id = " . $pnumber["product_id"];
$pdnumber = $db->GetRow($pd);

$u = "SELECT username FROM " . cms_db_prefix() . 'users WHERE user_id = ' . $pdnumber["owner"];
$uname = $db->GetOne($u);

$mod = \cms_utils::get_module("Products");

$out = array();
if ($_REQUEST['action'] == 'update') {
    $up = "UPDATE " . cms_db_prefix() . "module_products_history_seq SET id = " . $pnumber["id"];
    $db->Execute($up);
} else {
    if ($pnumber["id"] > $number) {
        $out["status"] = "OK";
        $out["sku"] = $pdnumber["sku"];
        $out["owner"] = $uname;
        $out["modified_date"] = $pdnumber["modified_date"];
    }
}

header("Content-type:application/json; charset=utf-8");

$handlers = ob_list_handlers();
for ($cnt = 0; $cnt < sizeof($handlers); $cnt++) {
    ob_end_clean();
}

echo json_encode($out);
exit;