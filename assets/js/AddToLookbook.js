﻿//This script file should be included ONLY ONCE in a page!

$(document).ready(function () {
    // When lookbook selected from dropdownlist
    $('div.divAddToLookbook ul.dropdown a[dbid]').live('click', function (e) {
        var lookbookId = $(this).attr('dbid');
        var divAddToLookbook = $(this).parents(".divAddToLookbook");
        var itemId = $('input:hidden[id$="hidItemId"]', divAddToLookbook).val();

        if ($.browser.msie && ($.browser.version == "6.0" || $.browser.version == "7.0")) {
            //don't show the fuzz.
        } else {
            $('#fuzz').slideDown(); //.css('zIndex', '4');
            $('#fuzz').height($(document).height());
        }
        $('#searchPanel, #advancedSearch, #lookLinks, #signInLinks, #menu').css('visibility', 'hidden');

        var productName = $('label[id$="lblProductName"]', divAddToLookbook).text();
        $('#lblCurrentLookBookProductName').text(productName);

        if ($(this).parent().is(":last-child")) {
            $('#lblCurrentLookBookItemId').text(itemId);
            $('#divSelectOrCreateLookbook').show().centerInClient();
            $('p.errorMsg', $('#divSelectOrCreateLookbook')).text("");
        } else {
            var lookbookName = $(this).text();
            var oldLookbookId = $('input:hidden[id$="hidOldLookbookId"]', divAddToLookbook).val();

            $('#lblCurrentLookbookName').text(lookbookName);
            $('#btnGoToLookbook').attr('href', '/Lookbook.aspx?id=' + lookbookId);
            if (oldLookbookId == "")
                PageMethods.AddItemToLookBook(lookbookId, itemId, AddItemToLookBookSuccess, AddItemToLookBookFailed);
            else
                PageMethods.MoveItemToLookBook(lookbookId, itemId, oldLookbookId, AddItemToLookBookSuccess, AddItemToLookBookFailed);
        }
        e.preventDefault();
    });

    // When Save button clicked
    $('#btnSelectOrCreateLookbookSave').live('click', function (e) {
        if ($('#divExistingLookbooks li.over').length == 1) { // If a lookbook has been selected from the list
            var liSelected = $('#divExistingLookbooks li.over')[0];
            var lookbookId = $('a', liSelected).attr('dbid');
            var itemId = $('#lblCurrentLookBookItemId').text();
            var lookbookName = $('a', liSelected).text();
            $('#lblCurrentLookbookName').text(lookbookName);
            $('#btnGoToLookbook').attr('href', '/Lookbook.aspx?id=' + lookbookId);
            PageMethods.AddItemToLookBook(lookbookId, itemId, AddItemToLookBookSuccess, AddItemToLookBookFailed);
            //Reset the divExistingLookbooks
            $(liSelected).click();
        } else {
            var divOverlay = $('#divSelectOrCreateLookbook');
            var tbNewLookbookName = $('input:visible', divOverlay);
            var lookbookName = tbNewLookbookName.val().trim() != tbNewLookbookName.attr('defaultValue') ? tbNewLookbookName.val().trim() : '';
            if (lookbookName == '') {
				// tracking code addition
				_gaq.push(['_trackEvent','Product_Detail','Error','Add_To_New_Lookbook']);
                $('p.errorMsg', divOverlay).text("Please select or create a Lookbook.");
            } else {
                var itemId = $('#lblCurrentLookBookItemId').text();
                PageMethods.CreateLookbookWithItem(lookbookName, itemId, CreateLookbookWithItemSuccess, CreateLookbookWithItemFailed, divOverlay);
            }
        }

        e.preventDefault();
    });

    // When lookbook selected from the Existing Lookbooks list
    $('#divExistingLookbooks li').live('click', function (e) {
        e.preventDefault();
        $(this).siblings().removeClass('over');
        $(this).toggleClass('over');

        var tbNewLookbookName = $('input:visible', $('#divSelectOrCreateLookbook'));
        if ($(this).hasClass('over')) {
            tbNewLookbookName.attr("disabled", "disabled");
            tbNewLookbookName.val('');
        }
        else
            tbNewLookbookName.attr("disabled", "");
    });

    $('a.iconLookbook').live('click', function (event) {
        event.preventDefault();
        var productName = $('label[id$="lblProductName1"]', $(this).parent()).text();
        $('#ltlAnonymousLookbookProductName').text(productName + ' has been added to your Lookbook');

        var productId = $('label[id$="lblItemId1"]', $(this).parent()).text();
        PageMethods.AddItemToAnonymousLookBook(productId, AddItemToAnonymousLookBookSuccess, AddItemToAnonymousLookBookFailed, $(this));
    });

    $('input:visible', $('#divSelectOrCreateLookbook')).live("focus", function () {
        if (this.value == this.defaultValue) this.value = '';
    });

    $('input:visible', $('#divSelectOrCreateLookbook')).live("blur", function () {
        if ($(this).val().trim() == '') $(this).val('Name your Lookbook (maximum 32 characters)');
    });
});

function AddItemToLookBookSuccess(res) {
    $('#divSelectOrCreateLookbook').hide();
    $('#divItemAddedToLookbook').show().centerInClient();

    var productName = $.trim($('h2#productTitle').text());
    _gaq.push(['_trackEvent','Product_Detail','Lookbook_Add',productName]);
    if (res) {

        // Update the item count for the lookbook in ddlLookBook at the top of page
        var lookbookId = $('#btnGoToLookbook').attr('href').replace('/Lookbook.aspx?id=', '');
        var lookbookName = $('#lblCurrentLookbookName').text().trim();
        $('li', $('#ctl00_ddlLookBook_list')).each(function (index) {
            if ($('a', $(this)).attr('dbid') == lookbookId) {
                var count = $('a', $(this)).text().replace(lookbookName, '').trim().replace('(', '').replace(')', '');
                $('a', $(this)).text(lookbookName + ' (' + (count * 1 + 1) + ')');
                return;
            }
        });
    }
}
function AddItemToLookBookFailed(res) {
    alert(res.get_message());
}

// Successfuly created new lookbook with the item
function CreateLookbookWithItemSuccess(res, divOverlay) {
    _gaq.push(['_trackEvent','Product_Detail','CTA_Click','Lookbook_Create']);
    var productName = $.trim($('h2#productTitle').text());
    _gaq.push(['_trackEvent','Product_Detail','Add_To_New_Lookbook',productName]);

    var lookbookName = $('input:visible', divOverlay).val().trim();
    $('#lblCurrentLookbookName').text(lookbookName);

    $('#btnGoToLookbook').attr('href', '/Lookbook.aspx?id=' + res);

    $('input:visible', divOverlay).val("");
    divOverlay.hide();
    $('#divItemAddedToLookbook').show().centerInClient();
    $('#searchPanel, #advancedSearch, #lookLinks, #signInLinks, #menu').css('visibility', 'hidden');
    var prevLookbookCount = $('li', $('#divExistingLookbooks')).length;

    // Insert the newly created lookbook into the every lookbook dropdown in the page.  Note on some page,
    // like ShoppingCart.aspx, there can be many AddToLookbook user controls.
    var liHtml = '<li><a dbid="' + res + '">' + lookbookName + '</a></li>';
    $('div.divAddToLookbook').each(function () {
        if (prevLookbookCount >= 10) {
            $('ul.dropdown a:last', $(this)).text('See all Lookbooks / Create a new one');
        }
        $('ul.dropdown li', $(this)).each(function (index) {
            if ($('a', $(this)).text().trim().toLowerCase() > lookbookName.toLowerCase() || $(this).is(":last-child")) {
                $(this).before(liHtml);
                // Only show the first 10 lookbooks in dropdown
                if ($(this).siblings().length > 10) {
                    $('li:eq(10)', $(this).parent()).hide();
                }
                return false;
            }
        });
    });

    // Insert the newly created lookbook into the divExistingLookbooks on the page
    if (prevLookbookCount == 0) {
        $('ul', $('#divExistingLookbooks')).html(liHtml);
    }
    else {
        if (prevLookbookCount >= 10) {
            $('li[id$="liSelectLookbook"]', '#divSelectOrCreateLookbook').show();
        }
        $('li', $('#divExistingLookbooks')).each(function (index) {
            if ($('a', $(this)).text().trim().toLowerCase() > lookbookName.toLowerCase()) {
                $(this).before(liHtml);
                return false;
            } else if ($(this).is(":last-child")) {
                $(this).after(liHtml);
            }
        });
    }

    // Insert the newly created lookbook into ddlLookBook at the top of page
    var lblSelected = $('label[id$="lblSelected"]', $('#ctl00_ddlLookBook_divOne'));
    lblSelected.text('Lookbook (' + (prevLookbookCount + 1) + ')');
    if (prevLookbookCount == 0) lblSelected.addClass('dropdown');
    liHtml = '<li><a dbid="' + res + '" ' + 'href="/Lookbook.aspx?id=' + res + '">' + lookbookName + ' (1)</a></li>';
    if (prevLookbookCount == 10) {
        $('a:last', $('#ctl00_ddlLookBook_list')).text('See all Lookbooks / Create a new one');
    }
    $('li', $('#ctl00_ddlLookBook_list')).each(function (index) {
        if ($('a', $(this)).text().trim().toLowerCase() > lookbookName.toLowerCase() || $(this).is(":last-child")) {
            $(this).before(liHtml);
            // Only show the first 10 lookbooks in dropdown
            if ($(this).siblings().length > 10) {
                $('li:eq(10)', $(this).parent()).hide();
            }
            return false;
        }
    });

    // Insert the newly created lookbook into the divSelectOrCreateLB at the top of page
    if (prevLookbookCount == 0) {
        $('ul', $('#divExistingLBs')).html(liHtml);
    }
    else {
        if (prevLookbookCount >= 10) {
            $('li[id$="liSelectLB"]', '#divSelectOrCreateLB').show();
        }
        $('li', $('#divExistingLBs')).each(function (index) {
            if ($('a', $(this)).text().trim().toLowerCase() > lookbookName.toLowerCase()) {
                $(this).before(liHtml);
                return false;
            } else if ($(this).is(":last-child")) {
                $(this).after(liHtml);
            }
        });
    }
}

function CreateLookbookWithItemFailed(res, divOverlay) {
    var returnedMsg = res.get_message();
    _gaq.push(['_trackEvent','Product_Detail','Error','Add_To_New_Lookbook']);
    $('p.errorMsg', divOverlay).text(res.get_message());
}

function AddItemToAnonymousLookBookSuccess(res, divAddToLookbook) {    
	$('#fuzz').height($(document).height()).slideDown('fast', function () {
        $('#lookbookAddNoReg').show().centerInClient();
    });

    $('label[id$="ddlLookBook_lblSelected"]').html('<a href="/Lookbook.aspx">Lookbook (' + res + ')</a>');
}

function AddItemToAnonymousLookBookFailed(res, divAddToLookbook) {
    alert(res.get_message());
}





