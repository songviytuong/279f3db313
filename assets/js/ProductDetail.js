﻿var numberAddedEmails = 0;
var _idsFilteredByBedSize = null;
var _idsFilteredByOption = new Array(3);

$(document).ready(function () {
    // When page is first loaded, limit the Options dropdowns according to the selected IDs of the previous Option dropdowns
    if ($('ul[id$="ddlBedSizes_list"]').length > 0) {
        $.each($('ul[id$="ddlBedSizes_list"] a'), function (index, lnk) {
            if ($(this).text() == $('label[id$="ddlBedSizes_lblSelected"]').text()) {
                _idsFilteredByBedSize = $(this).attr('dbid').split(',');
                UpdateDdlOptions(0);
            }
        });
    }
    //For the first two Option dropdowns, calculate the _idsFilteredByOption and filter the following Option dropdown.
    for (var x = 0; x <= 1; x++) {
        $.each($('ul[id$="ddlOption_list"]:eq(' + x + ') a'), function (index, lnk) {
            if ($(this).text() == $('label[id$="ddlOption_lblSelected"]:eq(' + x + ')').text()) {
                _idsFilteredByOption[x] = $(this).attr('dbid').split(',');
                UpdateDdlOptions(x + 1);
            }
        });
    }

    var isMetricDisplayed = $('label[id$="DDLstUnits_lblSelected"]').text() == 'cm';
    if (isMetricDisplayed) {
        $(".Imperial").hide();
        $(".Metric").show();
    }

    // //Allow user to Toggle Units of Measure
    $("#unitDD ul.dropdown a").click(function () {
        var isMetricDisplayed = $('label[id$="DDLstUnits_lblSelected"]').text() == 'cm';
        var isMetricNew = $(this).text().trim() == 'cm';

        if (isMetricDisplayed != isMetricNew) {
            $(".Imperial").toggle();
            $(".Metric").toggle();
        }

        // When size unit changed, widthMin/widthMax etc used in Search.aspx page becomes not applicable, need to clear them.
        var isMetricOld = $.cookie('isMetric') == 'true';
        if (isMetricOld != isMetricNew) {
            $.cookie('isMetric', isMetricNew);
            $.cookie('widthMin', null);
            $.cookie('widthMax', null);
            $.cookie('heightMin', null);
            $.cookie('heightMax', null);
            $.cookie('depthMin', null);
            $.cookie('depthMax', null);
        }
    });

    $('.lookBookList div.dropdown').mouseleave(function () {
        $(this).prev('label').removeClass('lb-over');
        $(this).prev('label').addClass('lb-out');
    });

    $('.lookBookList label.dropdown').mouseover(function () {
        $(this).removeClass('lb-out');
        $(this).addClass('lb-over');
    });

    $('#AimgEmail').click(function () {
        if ($.browser.msie && ($.browser.version == "6.0" || $.browser.version == "7.0")) {
            //don't show the fuzz.
        } else {
            $('#fuzz').slideDown().height($(document).height()).css('zIndex', '4');
        }
        $('#pValidationErrorEmail').hide();
        $("#tblAddEmails").empty();
        $(".addAnch").show();
        numberAddedEmails = 0;
        //clear txtbox
        $('input[id$="tbRecipientName"]').val("");
        $('input[id$="tbRecipientName"]').removeClass("highlight");
        $('input[id$="tbReciepientEmail"]').val("");
        $('input[id$="tbReciepientEmail"]').removeClass("highlight");
        $('input[id$="tbYourName"]').val("");
        $('input[id$="tbYourName"]').removeClass("highlight");
        $('input[id$="tbYourEmail"]').val("");
        $('input[id$="tbYourEmail"]').removeClass("highlight");
        //$('input[name$="tbMessage"]').val("");
        $('.idleField').val('');
        $('input[id$="chkCopyToYou"]').attr('checked', "");

        $('#emailOverlay').show().centerInClient();
        $('#lookLinks, #signInLinks, #menu').css('visibility', 'hidden');

    });

    $('.lookLink').click(function () {
        $('#emailConfirmationOverlay').hide();
        $('#fuzz').hide();
    });

    $("a.expandStory").click(function (event) {
        if ($(this).siblings('.storyShort').is(':visible')) {
            $(this).siblings(".storyShort").hide();
            $(this).siblings(".storyLong").show();
            $(this).text('- Read less');
        } else {
            $(this).siblings(".storyShort").show();
            $(this).siblings(".storyLong").hide();
            $(this).text('+ Read more');
        }
        return false;
    });

    //The ddlSpecialGroups does not cause cascading filtering, but the ddlBedSizes and ddlOptions will cause cascading filtering.
    //Selection in ddlBedSizes will trigger filtering of the (up to 3) ddlOption, selection in the first ddlOption will trigger filtering of the 2nd and 3rd ddlOption, 
    //and selection in the 2nd ddlOption will trigger filtering of the 3rd ddlOption.
    $('ul[id$="ddlBedSizes_list"] li').click(function (event) {
        event.preventDefault();
        var lnk = $('a', $(this));
        _idsFilteredByBedSize = lnk.attr('dbid').split(',');
        var intersection = OuterJoin(_idsFilteredByBedSize, _idsFilteredByOption[0]);
        intersection = OuterJoin(intersection, _idsFilteredByOption[1]);
        intersection = OuterJoin(intersection, _idsFilteredByOption[2]);
        if (intersection.length == 1) {
            var queryStrings = GetQueryStrings();
            //When filtering narrows down a single item ID, redirect to display that item
            if (queryStrings['itemid'] != _idsFilteredByBedSize[0]) {
                $('#fuzz').slideDown('fast');
                window.location.href = "/ProductDetail.aspx?itemid=" + intersection;
                return;
            }
        }
        else {
            var queryStrings = GetQueryStrings();
            //When filtering narrows down a single item ID, redirect to display that item
            if (queryStrings['itemid'] != _idsFilteredByBedSize[0]) {
                $('#fuzz').slideDown('fast');
                window.location.href = "/ProductDetail.aspx?itemid=" + _idsFilteredByBedSize[0];
                return;
            }
        }

        UpdateDdlOptions(0);
    });

    $('ul[id$="ddlOption_list"] li').click(function (event) {
        event.preventDefault();
        var lnk = $('a', $(this));
        var index = $('#availableOptions div[id$="ddlOption_divOne"]').index($(this).parents('div[id$="ddlOption_divOne"]'));
        _idsFilteredByOption[index] = lnk.attr('dbid').split(',');
        var intersection = OuterJoin(_idsFilteredByBedSize, _idsFilteredByOption[0]);
        intersection = OuterJoin(intersection, _idsFilteredByOption[1]);
        intersection = OuterJoin(intersection, _idsFilteredByOption[2]);
        //        if (intersection == undefined || intersection == null) {
        //            var queryStrings = GetQueryStrings();
        //            //When filtering narrows down a single item ID, redirect to display that item
        //            if (queryStrings['itemid'] != _idsFilteredByOption[index][0]) {
        //                $('#fuzz').slideDown('fast');
        //                window.location.href = "/ProductDetail.aspx?itemid=" + _idsFilteredByOption[index][0];
        //                return;
        //            }
        //        }
        if (intersection != undefined && intersection != null) {
            if (intersection.length >= 1) {
                var queryStrings = GetQueryStrings();
                //When filtering narrows down a single item ID, redirect to display that item
                if (queryStrings['itemid'] != intersection[0]) {
                    $('#fuzz').slideDown('fast');
                    window.location.href = "/ProductDetail.aspx?itemid=" + intersection[0];
                    return;
                }
            }
        }
        //        if (intersection.length > 1) {
        //            var queryStrings = GetQueryStrings();
        //            //When filtering narrows down a single item ID, redirect to display that item
        //            if (queryStrings['itemid'] != intersection[0]) {
        //                $('#fuzz').slideDown('fast');
        //                window.location.href = "/ProductDetail.aspx?itemid=" + intersection[0];
        //                return;
        //            }
        //        }
        UpdateDdlOptions(index + 1); //Update the ddlOption starting from the next one
    });

    //Each available size item has a corresponding itemId, redirect to display that item
    $('ul[id*="ddlAvailableSizes"] li').click(function (event) {
        event.preventDefault();
        var lnk = $('a', $(this));
        var itemId = lnk.attr('dbid').split(',');
        $('#fuzz').slideDown('fast');
        window.location.href = "/ProductDetail.aspx?itemid=" + itemId;
    });

    //Each special group item has a corresponding itemId, redirect to display that item
    $('ul[id$="ddlSpecialGroups_list"] li').click(function (event) {
        event.preventDefault();
        var lnk = $('a', $(this));
        var itemId = lnk.attr('dbid').split(',');
        $('#fuzz').slideDown('fast');
        window.location.href = "/ProductDetail.aspx?itemid=" + itemId;
    });

    $('a[id$="lnkRequestQuote"]').live('click', function () {
        _gaq.push(['_trackEvent', 'Lead_Request_Quote', 'Start', reporting.getProductName()]);

        $('#fuzz').slideDown('fast', function () {
            $('#fuzz').height($(document).height());
            $('#requestQuate').show().centerInClient();
        });
    });
});

//Update all the ddlOption starting from the startIndex one
function UpdateDdlOptions(startIndex) {
    try {
        if (startIndex == 0 && _idsFilteredByBedSize == null) return;
        var filteredIds = _idsFilteredByBedSize;

        $.each($('div[id$="ddlOption_divOne"]'), function (index1, div) {
            if (index1 < startIndex) {
                filteredIds = OuterJoin(filteredIds, _idsFilteredByOption[index1]);
                return true;
            }
            var isSelectedValid = false;
            var selectedText = $('label[id$="ddlOption_lblSelected"]', $(this)).text();
            $.each($('ul[id$="ddlOption_list"] li', $(this)), function (index2, li) {
                var ids = $('a[dbid]', li).attr('dbid').split(',');
                var intersection = InnerJoin(filteredIds, ids);
                if (intersection != null) {
                    $(li).show();
                    if ($('a[dbid]', li).text() == selectedText) {
                        isSelectedValid = true;
                    }
                } else {
                    $(li).hide();
                }
            });

            // Clear the selected text if it is not among the visible options anymore

            if (!isSelectedValid) {
                $('label[id$="ddlOption_lblSelected"]', $(this)).text('Select an Option');
                _idsFilteredByOption[index] = null;
            }
        });
    } catch (e) { }
}

//Get an array containing the common items from two arrays.  Treat null array as 'all inclusive', i.e., return the other array.
function OuterJoin(array1, array2) {
    if (array1 == null) return array2;
    if (array2 == null) return array1;

    return InnerJoin(array1, array2);
}
// Get an array containing the common items from two arrays
function InnerJoin(array1, array2) {
    var intersection = '';
    for (i1 in array1) {
        for (i2 in array2) {
            if (array1[i1].toLowerCase() == array2[i2].toLowerCase()) intersection += (intersection == '' ? '' : ',') + array1[i1];
        }
    }

    if (intersection == '') return null;
    return intersection.split(',');
}

function GetQueryStrings() {
    // get querystring as an array split on "&"
    var querystring = location.search.replace('?', '').split('&');
    // declare object
    var queryObj = {};
    // loop through each name-value pair and populate object
    for (var i = 0; i < querystring.length; i++) {
        // get name and value
        var name = querystring[i].split('=')[0].toLowerCase();
        var value = querystring[i].split('=')[1];
        // populate object
        queryObj[name] = value;
    }
    return queryObj;
}

function CheckTheUnit() {
    if ($("#unitDD label.dropdown").text() == "cm") {
        $(".Imperial").hide();
        $(".Metric").show();
    }
    else {
        $(".Imperial").show();
        $(".Metric").hide();
    }
}

function ValidateControls() {
    var isValidated = true;
    $('#pValidationErrorEmail').html('<label>Please correct the highlighted fields before continuing:</label><ul></ul>');
    isValidated &= ValidateRequiredField($('input[id$="tbRecipientName"]'), 'Please enter the recipient\'s name.', 'pValidationErrorEmail');
    isValidated &= ValidateRequiredField($('input[id$="tbReciepientEmail"]'), 'Please enter the recipient\'s email.', 'pValidationErrorEmail');
    isValidated &= ValidateEmail($('input[id$="tbReciepientEmail"]'), 'pValidationErrorEmail');
    for (var i = 1; i <= numberAddedEmails; i++) {
        isValidated &= ValidateRequiredField($('input[id$="lblRecName' + i + '"]'), 'Please enter the recipient\'s ' + i + ' name.', 'pValidationErrorEmail');
        isValidated &= ValidateRequiredField($('input[id$="tbRecEmail' + i + '"]'), 'Please enter the recipient\'s ' + i + ' email.', 'pValidationErrorEmail');
        isValidated &= ValidateEmail($('input[id$="tbRecEmail' + i + '"]'), 'pValidationErrorEmail');
    }
    isValidated &= ValidateRequiredField($('input[id$="tbYourName"]'), 'Please enter your name.', 'pValidationErrorEmail');
    isValidated &= ValidateRequiredField($('input[id$="tbYourEmail"]'), 'Please enter your email.', 'pValidationErrorEmail');
    isValidated &= ValidateEmail($('input[id$="tbYourEmail"]'), 'pValidationErrorEmail');

    if (!isValidated){
        $('#pValidationErrorEmail').show();
		_gaq.push(['_trackEvent','Product_Detail','Error','All_Fields']);
    } else {
        var email = "";
        var recNames = $('input[id$="tbRecipientName"]').val();
        var recEmails = $('input[id$="tbReciepientEmail"]').val();
        for (var j = 1; j <= numberAddedEmails; j++) {
            email += $('input[id$="lblRecName' + j + '"]').val() + '|' + $('input[id$="tbRecEmail' + j + '"]').val() + ',';
            recNames += ', ' + $('input[id$="lblRecName' + j + '"]').val();
            recEmails += ', ' + $('input[id$="tbRecEmail' + j + '"]').val();
        }
        $('input[id$="HiddenExtraEmails"]').val(email);
        $('#pValidationErrorEmail').hide();
        $('label[id$="lblRecNameConf"]').text("");
        $('label[id$="lblRecEmailConf"]').text("");
        $('label[id$="lblRecNameConf"]').text(recNames);
        $('label[id$="lblRecEmailConf"]').text(recEmails);
		_gaq.push(['_trackEvent', 'Product_Detail', 'Email_Sent', $('#productInfo h2#productTitle span').text()]);
    }
    return isValidated;
}

function AddMoreEmail() {
    if (numberAddedEmails > 4) {
        alert('The limit is reached!');
        return;
    }

    numberAddedEmails++;
    var tbody = $("#tblAddEmails");
    var trow = $("<tr>");
    var cell1Text = "Recipient's Name*"
    $("<td>").addClass("title").text(cell1Text).appendTo(trow);
    var cell2Text = "";
    $("<td>").addClass("value").text(cell2Text).append().html('<input type="text" class="idleField" id="lblRecName' + numberAddedEmails + '" value="" >').appendTo(trow);

    var trow2 = $("<tr>");
    var cell3Text = "Recipient's Email*";
    $("<td>").addClass("title").text(cell3Text).appendTo(trow2);
    var cell4Text = "";
    $("<td>").addClass("value").text(cell4Text).append().html('<input type="text" class="idleField" id="tbRecEmail' + numberAddedEmails + '" value="" >').appendTo(trow2);

    trow.appendTo(tbody);
    trow2.appendTo(tbody);

    if (numberAddedEmails >= 4) {
        $(".addAnch").hide();
    }
}

function CloseEmailOverlay() {
    $('#pValidationErrorEmail').hide();
    var divToClose = $('#emailOverlay');
    divToClose.hide();
    $('#fuzz').slideUp('fast', function () {
        $('#content, #lookLinks, #signInLinks, #menu').css('visibility', 'visible');
    }).css('zIndex', '2');
    //event.preventDefault();
}

