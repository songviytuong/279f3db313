﻿
function ValidateRequiredField(control, errMsg, errorDisplay) {
    var content = control.is('label') ? control.text().trim() : control.val();
    if (content == "") {
        control.addClass(control.is('label') ? "labelHighlight" : "highlight");
        if (document.activeElement.nodeName == "BODY") control.focus();
        if (errorDisplay == null ) errorDisplay = 'pValidationError';
        $('#' + errorDisplay+ ' ul').append('<li>' + errMsg + '</li>');
        return false;
    }
    else {
        control.removeClass("highlight");
        return true;
    }
}

function ValidateRadioButtonList(control, errMsg, errorDisplay) {
    if (!$('input:checked', control).val()) {
        $('input', control).addClass("highlight");
        if (document.activeElement.nodeName == "BODY") control.focus();
        if (errorDisplay == null) errorDisplay = 'pValidationError';
        $('#' + errorDisplay + ' ul').append('<li>' + errMsg + '</li>');
        return false;
    }
    else {
        $('input', control).removeClass("highlight");
        return true;
    }
}

function ValidateEmail(control, errorDisplay) {
    if (control.val() != "") {
        //var r = new RegExp('^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$','i');
        var r = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (!r.test(control.val())) {
            control.addClass("highlight");
            if (document.activeElement.nodeName == "BODY") control.focus();
            if (errorDisplay == null) errorDisplay = 'pValidationError';
            $('#' + errorDisplay + ' ul').append('<li>Email address: Enter a valid email address.</li>');
            return false;
        }
        else {
            control.removeClass("highlight");
        }
    }
    return true;
}

function ValidateMatch(control1, control2, title, errorDisplay) {
    if (control2.val() != "") {
        if (control1.val() != control2.val()) {
            control2.addClass("highlight");
            if (document.activeElement.nodeName == "BODY") control2.focus();
            if (errorDisplay == null) errorDisplay = 'pValidationError';
            $('#' + errorDisplay + ' ul').append('<li>' + title + '</li>');
            return false;
        }
        else {
            control2.removeClass("highlight");
        }
    }
    return true;
}

function ValidateInteger(control, errMsg, errorDisplay) {
    if (control.val() != "") {
        if (!/^\d+$/.test(control.val())) {
            control.addClass("highlight");
            if (document.activeElement.nodeName == "BODY") control.focus();
            if (errorDisplay == null) errorDisplay = 'pValidationError';
            //$('#' + errorDisplay + ' ul').append('<li>' + title + ' has to be an Integer.</li>');
            $('#' + errorDisplay + ' ul').append('<li>' + errMsg + '</li>');
            return false;
        }
        else {
            control.removeClass("highlight");
        }
    }
    return true;
}

function ValidatePhoneNumber(control, errorDisplay) {
    if (control.val() != "") {
        var r = /^[\d\-\(\)\ ]+$/;
        if (!r.test(control.val())) {
            control.addClass("highlight");
            if (document.activeElement.nodeName == "BODY") control.focus();
            if (errorDisplay == null) errorDisplay = 'pValidationError';
            $('#' + errorDisplay + ' ul').append('<li>Invalid phone number.</li>');
            return false;
        }
        else {
            control.removeClass("highlight");
        }
    }
    return true;
}

function ValidateRange(control, minValue, maxValue, errMsg, errorDisplay) {
    var value = parseInt(control.val());
    if (value < minValue || value > maxValue) {
        control.addClass("highlight");
        if (document.activeElement.nodeName == "BODY") control.focus();
        if (errorDisplay == null) errorDisplay = 'pValidationError';
        $('#' + errorDisplay + ' ul').append('<li>' + errMsg + '</li>');
        return false;
    }
    else {
        control.removeClass("highlight");
        return true;
    }
}

function ValidateCompare(thisControl, otherControl, operator, errMsg, errorDisplay) {
    var thisValue = parseInt(thisControl.val());
    var otherValue = parseInt(otherControl.val());
    var isSuccess = false;

    switch (operator) {
        case '<':
            isSuccess = thisValue < otherValue;
            break;
        case '<=':
            isSuccess = thisValue <= otherValue;
            break;
        case '>':
            isSuccess = thisValue > otherValue;
            break;
        case '>=':
            isSuccess = thisValue >= otherValue;
            break;
        default:
    }

    if (!isSuccess) {
        thisControl.addClass("highlight");
        if (errorDisplay == null) errorDisplay = 'pValidationError';
        if (document.activeElement.nodeName == "BODY") thisControl.focus();
        $('#' + errorDisplay + ' ul').append('<li>' + errMsg + '</li>');
        return false;
    }
    else {
        thisControl.removeClass("highlight");
        return true;
    }
}

