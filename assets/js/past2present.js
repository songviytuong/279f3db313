﻿$(window).load(function() {
    var arrayOfImages = [];
    $('#products td a').each(function() { arrayOfImages.push($(this).attr('rel')) });

    $(arrayOfImages).each(function() {
        (new Image()).src = this;
    });
});

$(document).ready(function() {

    $('#products a').hover(
        function() {
            $(this).find('div.rollover').fadeIn(600);
        },
        function() {
            $(this).find('div.rollover').fadeOut(200);
        }
    );
        $('#products a').click(function () {
        $(this).colorbox({ innerWidth: 870, innerHeight: 578, iframe: true, scrolling: false, escKey:false });
    });

    $('li.headlink').hover(
			function() { $('ul', this).css('display', 'block'); },
			function() { $('ul', this).css('display', 'none'); });



});


