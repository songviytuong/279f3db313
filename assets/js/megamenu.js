﻿// MegaMenu
var preloaded = new Array();
	
var megamenu = {
	on: false,
	open: function() {
		clearTimeout(megamenu.timeout);
		if ( megamenu.on != true ) {			
			megamenu.on = true;
			$('#fuzz').fadeIn(300);
			
			//Hack to deal with IE z-index hide everything with class .hideformenu
			if ($.browser.msie && $.browser.version <= 7.0) {
				$('.hideformenu',$('#content')).css("visibility", "hidden");
			}
		}
		$(this).find('div.megaContent').fadeIn(300);
		$(this).addClass("selected");
	},
	close: function() {
		clearTimeout(megamenu.timeout);
		$(this).removeClass("selected");
		$(this).find('div.megaContent').fadeOut(200);
		megamenu.timeout = setTimeout('megamenu.fuzzOff()',500);
	},
	preload: function() { //This is a generic preload function, could be moved to a global object outside of megamenu
		for (var i = 0; i < arguments.length; i++){
			preloaded[i] = document.createElement('img');
			preloaded[i].setAttribute('src',arguments[i]);
		};
	},
	timeout: null,
	fuzzOff: function() {
		megamenu.on = false;
		
		$(".megaContent").each( function(index,el) {
			if($(this).css("display") === "block") megamenu.on = true;
		});
		
		if(!megamenu.on) {
			$('#fuzz').fadeOut(200);
			
			//Hack to deal with IE z-index hide everything with class .hideformenu
			if ($.browser.msie && $.browser.version <= 7.0) {
				$('.hideformenu',$('#content')).css("visibility", "visible")
			}
		}
	}
}

// ZONE : MEGA MENU
$(document).ready(function() {
	

	//Preload menu images for faster display
	megamenu.preload(		
		//'/assets/images/bkg-menu-catalog.jpg',		
	);
	
	//Extra JS for Styles menu: these should be added to megamenu object above.
	function addAlthorp() {
        var replacementText = "<a href='/CategoryListing.aspx?collectionid=BD17641E-51F6-4B1A-8CA9-7DB8C5F27A74'>Reproductions of the Spencer Family ancestral home.</a>";
        $('#collectionLogos .althorp').html(replacementText);
    }
    function removeAlthorp() {
        var replacementLogo = "<a href='/CategoryListing.aspx?collectionid=BD17641E-51F6-4B1A-8CA9-7DB8C5F27A74'><img src='/assets/images/logo-althorp.png' alt='Althorp Logo'/></a>";
        $('#collectionLogos .althorp').html(replacementLogo);
    }
    function addKeno() {
        var replacementText = "<a href='/CategoryListing.aspx?collectionid=E3DCEC20-B23E-4D30-818E-1E04408585A9'>Traditional styling enhanced with fresh, modern appeal.</a>";
        $('#collectionLogos .keno').html(replacementText);
    }
    function removeKeno() {
        var replacementLogo = "<a href='/CategoryListing.aspx?collectionid=E3DCEC20-B23E-4D30-818E-1E04408585A9'><img src='/assets/images/logo-keno.png' alt='Keno Logo'/></a>";
        $('#collectionLogos .keno').html(replacementLogo);
    }

    $('.keno').hover(function() {
        $(this).children('a.keno-over').show();
        $(this).children('a.keno-out').hide();
    }, function() {
        $(this).children('a.keno-over').hide();
        $(this).children('a.keno-out').show();
    });
    $('.althorp').hover(function() {
        $(this).children('a.althorp-over').show();
        $(this).children('a.althorp-out').hide();

    }, function() {
        $(this).children('a.althorp-over').hide();
        $(this).children('a.althorp-out').show();
    });

	
	//Add menu actions
    var megaConfig = { interval: 0, sensitivity: 7, over: megamenu.open, timeout: 100, out: megamenu.close };
    $("#menu li.mega").hoverIntent(megaConfig);	
});

