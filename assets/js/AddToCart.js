﻿$(document).ready(function () {
    $('a.btnCart').live('click', function () {
        var divAddToCart = $(this).parents('div.AddToCart');
        var tbQuantity = $('input.inputQty', divAddToCart);
        var itemId = $('input[id$="hidItemId"]', divAddToCart).val();

        if (tbQuantity.val() == "") {
            AddToShoppingCartFailed("Please enter a quantity.", divAddToCart);
            _gaq.push(['_trackEvent','Product_Detail','Add_to_Cart_Error','Unspecified_Quantity']);
            return false;
        }
        var newQuantity = parseInt(tbQuantity.val());
        if (isNaN(newQuantity) || newQuantity <= 0 || tbQuantity.val().indexOf(".") != -1) {
            AddToShoppingCartFailed("Please enter a positive number to specify quantity.", divAddToCart);
            _gaq.push(['_trackEvent','Product_Detail','Add_to_Cart_Error','Non_Numeric_Quantity']);
            return false;
        }

        var quantityMultiplier = $('input[id$="hidQuantityMultiplier"]', divAddToCart).val();
        if (newQuantity % quantityMultiplier != 0) {
            AddToShoppingCartFailed("Please adjust your quantity.", divAddToCart);
            _gaq.push(['_trackEvent','Product_Detail','Add_to_Cart_Error','Quantity_Multiplier']);
            return false;
        }
        $('#fuzz').slideDown('fast');

        var productName = $.trim($(this).parents('#productInfo').find('h2').text());
        _gaq.push(['_trackEvent','Shopping_Cart','Add_To_Cart',productName,newQuantity]);

        // call server side method, note divAddToCart is passed as last parameter so that AddToShoppingCartSuccess & AddToShoppingCartFailed can reference it.
        PageMethods.AddToShoppingCart(itemId, newQuantity, quantityMultiplier, AddToShoppingCartSuccess, AddToShoppingCartFailed, divAddToCart);
    });

    $('.btnContinueShopping').live('click', function (e) {
        e.preventDefault();

        curDiv = $(this).parents('.AddToCartConfirmation');
        $('#fuzz').slideUp('slow', function () {
            $(curDiv).hide();
        }).css('zIndex', '2');
    });
});

// CallBack function if static method call is successful
function AddToShoppingCartSuccess(res, divAddToCart) {
    // remove error message and highlighting of the Quantity input field
    var msg = $('p.errorMsg', divAddToCart);
    var input = $('input.inputQty', divAddToCart);
    var quantity = input.val();
    msg.text("");
    input.removeClass('errorQty');
    input.val("");

    //update the master page shopping cart
    $("#ctl00_lnkShoppingCart").text('Shopping cart (' + res + ')');

    var divConfirmation = $('#divAddToCartConfirmation');
    var price = parse($('span[id$="lblWholeSalePrice"]', divAddToCart).text());
    $('.lblUnitPrice', divConfirmation).text("$" + number_format(price));
    $('.lblQuantity', divConfirmation).text(quantity);
    $('.lblItemTotal', divConfirmation).text("$" + number_format(quantity * price));
    $('#lblAddToCartProductName').text($('label[id$="lblProductName"]', divAddToCart).text());
    $('#lblAddToCartSKU').text($('label[id$="lblSKU"]', divAddToCart).text());

//    var colors = $('label[id$="lblColor"]', divAddToCart).text();
//    if (colors.trim() != '') {
//        $('#ulAddToCartConfirmationOptions').show();
//        $('#lblAddToCartColor').text($('label[id$="lblColor"]', divAddToCart).text());
//    }
//    else
//        $('#ulAddToCartConfirmationOptions').hide();

    $('#imgAddToCartItem').attr('src', $('#lblScene7ImageRoot').text() + $('label[id$="lblDefaultCode"]', divAddToCart).text() + '_main_1?$ProductListList$');

    //$('div.productDetails ul.options', divConfirmation).html($('div.productDetails ul.options li:first', divConfirmation)[0].outerHTML);
    $('#ulAddToCartConfirmationOptions').html('');
    var options = $('label[id$="lblOptions"]', divAddToCart).text().split(';');
    if (options[0].length > 0) {
        for (var i in options) {
            var tmp = options[i].split(':');
            $('#ulAddToCartConfirmationOptions').append('<li><strong>' + tmp[0].trim() + ' : </strong><label>' + tmp[1].trim() + '</label></li>');
        };
    }

    PageMethods.GetShoppingCartSummary(OnGetShoppingCartSummarySuccess, OnGetShoppingCartSummaryFailed, false);
}

// CallBack function if static method call failed
function AddToShoppingCartFailed(res, divAddToCart) {
    $('#fuzz').slideUp('slow'); //.css('zIndex', '2');
    var msg = $('p.errorMsg', divAddToCart);
    var input = $('input.inputQty', divAddToCart);
    if ((typeof res) == 'string')
        msg.text(res);
    else
        msg.text(res.get_message());

    input.addClass('errorQty');
    var quantityMultiplier = $('input[id$="hidQuantityMultiplier"]', divAddToCart).val();
    input.val(quantityMultiplier);
    input.focus();
}

function OnGetShoppingCartSummarySuccess(res, isBulk) {
    var rets = res.split('/');
    $('.lblTotalItems', $('#divAddToCartConfirmation')).text(rets[0]);
    $('.lblCartVolume', $('#divAddToCartConfirmation')).text(number_format(rets[1], 2) + " CBM");
    $('.lblCartSubtotal', $('#divAddToCartConfirmation')).text("$" + number_format(rets[2]));

    if (isBulk) {
        $('li.cartContents', $('#divAddToCartConfirmation')).hide();
        $('li.pricing', $('#divAddToCartConfirmation')).hide();
    } else {
        $('li.cartContents', $('#divAddToCartConfirmation')).show();
        $('li.pricing', $('#divAddToCartConfirmation')).show();
    }

    $('#divAddToCartConfirmation').show().centerInClient();
}

function OnGetShoppingCartSummaryFailed(res, isBulk) {
    $('#fuzz').slideUp();
    alert(res.get_message());
}

