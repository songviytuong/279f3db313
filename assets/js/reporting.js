﻿var reporting = {
    getPageTitle: function () {
        var pageTitle;
        var rawTitle = document.title;
        if (rawTitle == "Theodore Alexander") {
            pageTitle = "Home";
        } else {
            pageTitle = rawTitle.replace(/Theodore\ Alexander\ \-\ /, '');
        }

        return pageTitle;
    },
    priceToInteger: function (price) {
        if (typeof price == "string") {
            return price.replace(/\D/g, '');
        }
    },
    getRadioLabel: function (obj) {
        var radioLabel;
        radioLabel = $(obj).next('label').text();
        
        return radioLabel;
    },
    getProductName: function () {
        var productName = $.trim($('#productTitle span').text());
        return productName;
    },
    userIsLoggedIn: function() {
        if ($('#signInLinks #signinPanel a').text()=="Sign In") {
            return false;
        }
        return true;
    }
};

function flashTrack(method, category, action, label, playTime) {
	if(playTime != null) {
		_gaq.push([method, category, action, label, playTime]);
	} else {
		_gaq.push([method, category, action, label]);
	}
}

$(document).ready(function() {
    
    

		// Tracking on Dealer Locator //
		$('#googleMap li a.iconPrint').live("click", function (event) { _gaq.push(['_trackEvent','Dealer_Locator','Print',$("#googleMap .googleContent #spnStoreName").text().trim()]); });
		$('#locateDealer ul[id$=ddlContinent_list] li a').live("click", function (event) { _gaq.push(['_trackEvent','Dealer_Locator','Continent',$(this).text()]);});
		$('#locateDealer ul[id$=ddlCountry_list] li a').live("click", function (event) {_gaq.push(['_trackEvent','Dealer_Locator','Country',$(this).text()]);});
		$('#locateDealer ul[id$=ddlState_list] li a').live("click", function (event) {_gaq.push(['_trackEvent','Dealer_Locator','State_Prov',$(this).text()]);});
		$('#dlrResults a.storeName').live("click", function (event) {_gaq.push(['_trackEvent','Dealer_Locator','Overlay',$(this).text().trim()]);});
		$('#searchForm a.button[id$="btnSearchByStoreName"]').live("click", function (event) {_gaq.push(['_trackEvent','Dealer_Locator','Dealer_Name',$('#searchForm #searchByName input[id$="txtBoxStoreName"]').val().trim()]);});
		$('#searchForm a.button[id$="btnSearchByZip"]').live("click", function (event) {_gaq.push(['_trackEvent','Dealer_Locator','Zip',$('#searchForm #searchByZip input[id$="txtBoxZipCode"]').val().trim()]);});
		
		// Tracking on Lookbooks //	
		$('.lookBook #top li a.iconPrint').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Print','Lookbook']); });
		$('.lookBook #top li a.iconEmail').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Email','Lookbook']); });
		$('.lookBook a.button[id$="lnkBtnRegister"]').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','CTA_Click','Register']); });
		$('.lookBook #top li.quoteRequest a[id$="lnkRequestQuote"]').live("click", function (event) { _gaq.push(['_trackEvent','Lead_Request_Quote','Start','Lookbook_ProductName']); });
		$('.lookBook #selectOrder ul.dropdown li a[id$="ctrl0_btnItem"]').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Sort','Order_By_Newest_Date_Added']); });
		$('.lookBook #selectOrder ul.dropdown li a[id$="ctrl1_btnItem"]').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Sort','Order_By_Oldest_Date_Added']); });
		$('.lookBook #selectOrder ul.dropdown li a[id$="ctrl2_btnItem"]').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Sort','Order_Alphabetically_Category_Type']); });

		$('tr.row a.remove').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Remove',$.trim($(this).parents('tr.row').find('h1.productName').text())]); });
		$('#ctl00_ContentPlaceHolderMiddle_lnkEdit').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Link_Click',$.trim($('#ctl00_ContentPlaceHolderMiddle_lblLookbookName').text())]); });
		$('#editLookbookName.overlay .buttons a.button[id$="btnSave"]').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Note_Save','Lookbook']); });
		$('.lookBook .lookbookDropdown ul.dropdown li a[id$="hplItem"]').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Overlay','Copy_To_Lookbook']); });
		$('.lookBook #top li a[id$="lnkCart"]').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Add_To_Cart',$.trim($(this).closest('tr.row').find('h1.productName a').text())]); });
		$('.PurchaseView .AddToCart .button').live("click", function (event) { _gaq.push(['_trackEvent','Lookbook','Quantity',$(".AddToCart input.inputQty").val()]); _gaq.push(['_trackEvent','Lookbook','Add_To_Cart',$.trim($(this).closest('tr.row').find('h1.productName a').text())]); });
		$('.lookBook .divAddToLookbook ul.dropdown li a[id$=hplItem]').live("click", function (event) {  _gaq.push(['_trackEvent','Lookbook','Copy_Piece',$.trim($(this).closest('tr.row').find('h1.productName a').text())]);  _gaq.push(['_trackEvent','Lookbook','CTA_Click', $(this).text().trim() ]); });
				
		// Tracking for Auxiliary pages //
		$('#tabs .ui-tabs-nav .ui-corner-top a').click(function () {_gaq.push(['_trackEvent','About_Us','Tab_Select',$(this).text()]); });
    

    /**
     * Navigation
     */
    // Collections CTA
    $('.collectionDiscover a').click( function() {
        _gaq.push(['_trackEvent','MM_CTA','Learn_About','Collections_Themes']);
    });
    // Styles CTA
    $('.stylesDiscover a').click( function() {
        _gaq.push(['_trackEvent','MM_CTA','Learn_About','Styles']);
    });
    // Sitemap link in footer
    $('#footer a:contains(SITE MAP)').click(function(){
        _gaq.push(['_trackEvent','Site_Map','Overlay','Footer']);    
    });

    /**
     *  My Account
     */
    // My account navigation tabs
    $('#account ul#tabNav a').click( function() {
        var linkHref = $(this).attr('href');
        var reportString;
        try {
            linkHref = linkHref.match(/\/account\/([^.]+)/)[1];

            switch (linkHref) {
                case 'accountsettings':
                    reportString = 'Settings';
                    break;
                case 'lookbooks':
                    reportString = 'Lookbooks';
                    break;
                case 'dealersettings':
                    reportString = 'Dealer';
                    break;
                case 'orderhistory':
                    reportString = 'Order';
                    break;
            } 
            
            _gaq.push(['_trackEvent','Account','Tab_Select',reportString]);
        }
        catch (e) {
            return false;
        }
    });

    // Edit account details button
    $('#account div[id$=udpAccount] a.btnEdit').live('click',function() {
        // Do not fire if this selector matches (Lookbooks page)
        if ($('.section table').length == 0) {
            _gaq.push(['_trackEvent','Account','CTA_Click','Settings_Edit']);
        }
    });

    // Save account details button
    $('#account [id$=pnlAboutButtons] a.button').live('click', function() {
        _gaq.push(['_trackEvent','Account','CTA_Click','Edit_Save_Changes']);
    });

    // Sales Associate learn more CTA
    $('#account div[id$=pnlSalesAssociate] a.learnmore').live('click', function() {
        _gaq.push(['_trackEvent','Account','Link_Click','Sales_Associate']);
    });

    // Sales Associate form submission
    $('div[id$=pnlEmailStore] a[id$=lnbEmailSend]').live('click',function() {
        _gaq.push(['_trackEvent','Account','Success','Sales_Associate']);
    });

    // Dealer settings edit
    $('#account div[id$=udpSettings] a.btnEdit').click(function(){
        _gaq.push(['_trackEvent','Account','CTA_Click','Dealer_Settings_Edit']);
    });

    // Lookbook create
    $('#account a[id$=lbnCreate]').live('click', function() {
        _gaq.push(['_trackEvent','Account','CTA_Click','Lookbook_Create']);
    });
    // Edit lookbooks
    $('#account div[id$=udpAccount] a.btnEdit').click(function() {
        if ($('#account h2:contains(Lookbooks)').length) {
            _gaq.push(['_trackEvent','Account','CTA_Click','Lookbook_Edit']); 
        }
    });

    // Order history print and export to Excel
    $('#account ul#export input[id$=lnkPrint]').click(function(){
        _gaq.push(['_trackEvent','Account','Print','Order_Status']);
    });
    $('#account ul#export input[id$=imbExport]').click(function(){
        _gaq.push(['_trackEvent','Account','Export','Order_Status']);
    });
    
    /**
     * Shopping Cart
     */
    // On sort order change
    $('div#cartHeader div[id$=ddlOrderBy_divTwo] a').click( function() {
        var reportString;
        reportString = $(this).text().replace('Sort by ','');
        _gaq.push(['_trackEvent','Shopping_Cart','Sort',reportString]);
    });

    // On copy to lookbook
    $('tr.cartItem div.divAddToLookbook div[id$=ddlLookbook_divTwo] a').click( function() {
        _gaq.push(['_trackEvent','Shopping_Cart','Overlay','Copy_To_Lookbook']);
        var productName = $(this).parents('td.pricingContent').prev().find('h1 a').text();
        _gaq.push(['_trackEvent','Shopping_Cart','Copy_Piece',productName]);
    });

    // On remove item
    $('tr.cartItem td.pricingContent a.remove').click( function() {
        var productName = $(this).parents('td.pricingContent').prev().find('h1 a').text();
        _gaq.push(['_trackEvent','Shopping_Cart','Remove',productName]);
    });

    // On add to cart
    // Quantity multiplier  error
    // Quantity not numeric
    // Quantity unspecified 
    // - in AddToCart.js
    
    // On shipping address help
    // - in BillingAndShipping.aspx

    // On verify your order btn click
    $('div.cartButtons a#btnVerify').click( function() {
        _gaq.push(['_trackEvent','Shopping_Cart','CTA_Click','Verify_Your_Order',]);
    });
    
    // Order summary page
    if (document.location.href.match(/CheckOut\/OrderReceived/) !== null) {
        // On print order summary
        $('a.iconPrint').click( function() {
            _gaq.push(['_trackEvent','Shopping_Cart','Print','Order_Summary']);
        });

        /**
         * GA E-commerce tracking
         */
        var OrderID;
        var OrderTotal;
        var OrderCity;
        var OrderState;
        // Get Order id
        OrderID = $('input#hdnOrderID').val();
        // Order total
        OrderTotal = reporting.priceToInteger($('span[id$=ltlSubtotal]').text());

        if (!$.browser.msie) {
            orderAddressArr =$('span[id$=lblShippingAddress]')[0].innerHTML.split('<br>'); 
        }
        else {
            orderAddressArr =$('span[id$=lblShippingAddress]')[0].innerHTML.split('<BR>'); 
        }
        // Order city
        OrderCity = orderAddressArr[2].substr(0,orderAddressArr[2].indexOf(','));
        // Order state
        OrderState = orderAddressArr[2].substr(orderAddressArr[2].indexOf(',')+2);

        // Begin transaction
        _gaq.push(['_addTrans',OrderID,'Theodore Alexander',OrderTotal,'','',OrderCity,OrderState]);

        // Add items to transaction
        $('table.cartContents tr.cartItem').each( function() {
            _gaq.push(['_addItem',
                OrderID,
                $.trim($(this).find('ul.sku li:first').text()),
                $(this).find('td.productInfo h1').text(),
                '',
                reporting.priceToInteger($(this).find('table.pricing td.col1').text()),
                $.trim($(this).find('table.pricing td.col2').text())
                ]);
        });

        // Submit transaction
        _gaq.push(['_trackTrans']);
    }


    // Report product detail page view event
    if ($('#productDetailPage').length) {
        var productName = $.trim($('h2#productTitle').text());
        _gaq.push(['_trackEvent','Product_Detail','Product_View',productName]);
    }


    // Show only newest
    $('input[id$=cbNewest]').not(':checked').click(function (e) {
        _gaq.push(['_trackEvent', 'Product_Listing', 'Filter', 'Newest']);
    });

    // FCA Explore link
    $('#fca ul li:eq(1)').click(function () {
        _gaq.push(['_trackEvent', 'FCA', 'CTA_Click', reporting.getPageTitle() + '_Explore']);
    });

    // Filter views
    // View grid
    $('li.btnGridView ul[id$=ddlGridView_list] li').live('click',function () {
        _gaq.push(['_trackEvent', 'Product_Listing', 'View', 'Grid']);
    });
    // View list
    $('li.btnListView ul[id$=ddlListView_list] li').live('click',function () {
        _gaq.push(['_trackEvent', 'Product_Listing', 'View', 'List']);
    });

    // Print button
    $('#productDetailPage #AimgPrinter').click(function () {
        _gaq.push(['_trackEvent', 'Product_Detail', 'Print', reporting.getProductName()]);
    });
    // Email button
    $('#AimgEmail').click(function () {
        _gaq.push(['_trackEvent', 'Product_Detail', 'Email_Product', reporting.getProductName()]);
    });


    // Unit selected (Metric or Imperial)
    $('#ContentPlaceHolderMiddle_DDLstUnits_list li a').click(function () {
        var unitSelected = $(this).text();

        switch (unitSelected) {
            case 'inches':
                unitSelected = 'Imperial';
                break;
            case 'cm':
                unitSelected = 'Metric';
                break;
            default:
                break;
        }

        _gaq.push(['_trackEvent', 'Product_Detail', 'Unit', unitSelected]);
    });

	// Click of Sign In link
    $('#signinPanel a.linkSignin').click(function() { _gaq.push(['_trackEvent', 'Sign_In', 'Link_Click', 'Header']); });

    // Click of Forgot username
    $('#signinContent a.forgotLink[id$="UserName"]').click(function() { _gaq.push(['_trackEvent', 'Sign_In', 'Forgot_Username', 'Header']); });

    // Click of Forgot password
    $('#signinContent a.forgotLink[id$="Password"]').click(function() { _gaq.push(['_trackEvent', 'Sign_In', 'Forgot_Password', 'Header']); });

    // Click of Cancel
    $('#signinContent a.btnCancel').click(function() { _gaq.push(['_trackEvent', 'Sign_In', 'Cancel', 'Header']);  });

    // Click of Close
    $('#divSignIn a.btnClose').click(function() { _gaq.push(['_trackEvent', 'Sign_In', 'Close', 'Header']); });

	$('#productInfo li#lookBookList').click( function() {
		var productName = $.trim($(this).parents('#productInfo').find('h2').text());
        if (reporting.userIsLoggedIn()===false) {
            _gaq.push(['_trackEvent','Product_Detail','Lookbook_Add',productName]);
        }
	});

	// 158
	$('#lookbookAddNoReg li.buttons a.button').live('click', function() {
		_gaq.push(['_trackEvent','Product_Detail','CTA_Click','Register']);
	});
	
    //Register  

    $('#registerAccount ul#ctl00_ContentPlaceHolderMiddle_ddlCountry_list li a').live("click", function () { _gaq.push(['_trackEvent', 'Registration', 'Country', $.trim($(this).text())]); });
    $('#registerAccount ul#ctl00_ContentPlaceHolderMiddle_ddlState_list li a').live("click", function () { _gaq.push(['_trackEvent', 'Registration', 'State_Prov', $.trim($(this).text())]); });
    $('#registerAccount ul#ctl00_ContentPlaceHolderMiddle_ddlCity_list li a').live("click", function () { _gaq.push(['_trackEvent', 'Registration', 'City', $.trim($(this).text())]); });
    $("#registerAccount .buttons a#btnCancel").click(function () { _gaq.push(['_trackEvent', 'Registration_Page', 'Cancel', 'Main']); });
    $("#registerLogin #ctl00_ContentPlaceHolderMiddle_btnForgotUsernameSubmit").click(function () { _gaq.push(['_trackEvent', 'Sign_In', 'Forgot_Username', 'Register']); });
    $("#registerLogin #ctl00_ContentPlaceHolderMiddle_btnForgotPasswordSubmit").click(function () { _gaq.push(['_trackEvent', 'Sign_In', 'Forgot_Password', 'Register']); });

    //House of TA    
    $('body.pasttopresent #products a').click(function () { _gaq.push(['_trackEvent', 'House_TA', 'Overlay', $(this).find('p').text()]); });
    $('body.craftsmanshipandartistry #craft-container #films ul li a').click(function () { _gaq.push(['_trackEvent', 'House_TA_Video', 'Main_Play', $.trim($(this).find('span.title').text())]); });

    $('body.craftsmanshipandartistry #craft-container #photos ul li a').live('click',function () { _gaq.push(['_trackEvent', 'House_TA', 'Overlay', 'Photo_'+$.trim($(this).find('span').text())]); });
    $('body.craftsmanshipandartistry #craft-container #tabs ul.ui-tabs-nav li a').click(function () { _gaq.push(['_trackEvent', 'House_TA', 'Tab_Select', $.trim($(this).text())]); });
    $('body.socialandenvironmental #house-ta-content #tabs ul.ui-tabs-nav li a').click(function () { _gaq.push(['_trackEvent', 'House_TA', 'Tab_Select', $.trim($(this).text())]); });


    // tag 102, 147
    $('div#cboxOverlay').live("click", function () {
        if ($('body.craftsmanshipandartistry #craft-container #tabs ul.ui-tabs-nav li.tab1').hasClass('ui-state-active')) {
            var iFrame = window.frames[1].document.getElementById('flashcontainer');
            iFrame.onOverlayClosed('House_TA_Video');
        }

        if ($('#productDetails div.ajax__tab_header span').hasClass('ajax__tab_active')) {
            var iFrame = window.frames[1].document.getElementById('flashcontainer');
            iFrame.onOverlayClosed('Behind_This_Piece_Video');
        }

        if ($('#fca').length) {
//            var iFrame = window.frames[1].document.getElementById('flashcontainer');
//            iFrame.onOverlayClosed('FCA_Video');
        }
    });

    $('div#colorbox div#cboxClose').live("click", function () {        
//        var iFrame = window.frames[1].document.getElementById('flashcontainer');
//        iFrame.onOverlayClosed('Behind_This_Piece_Video');        
    });


    // Catalogue - Product Detail Page

    $('#productDetailPage ul#ctl00_ContentPlaceHolderMiddle_DDLstUnits_list li a').click(function () { _gaq.push(['_trackEvent', 'Product_Detail', 'Unit', $.trim($(this).text())]); });
    $('#productDetailPage ul[id$=ddlSpecialGroups_list] li a,#productDetailPage ul[id$=ddlOption_list] li a,#productDetailPage ul[id$=ddlBedSizes_list] li a').click(function () { _gaq.push(['_trackEvent', 'Product_Detail', 'Option_Select', $.trim($(this).text())]); });
    $('#productDetailPage a#ctl00_ContentPlaceHolderMiddle_lnkBtnExplore').click(function () { _gaq.push(['_trackEvent', 'Product_Detail', 'CTA_Click', 'Explore']); });

    $('#productDetailPage a#lnkEnquiry').click(function (e) {        
        _gaq.push(['_trackEvent', 'Lead_Product_Inquiry', 'Start', reporting.getProductName()]);
        setTimeout("window.location = '/ContactUs.aspx' ", 500); 
    });

    $('#productDetailPage ul#productsList li a').click(function () { _gaq.push(['_trackEvent', 'Product_Detail', 'CTA_Click', 'May_Also_Like']); });
    $('#productDetailPage #__tab_ctl00_ContentPlaceHolderMiddle_ProductDetailTabs_details').click(function () { _gaq.push(['_trackEvent', 'Product_Detail', 'Tab_Select', reporting.getProductName() + '_Details']); });    
    $('#productDetailPage #__tab_ctl00_ContentPlaceHolderMiddle_ProductDetailTabs_behind').click(function () { _gaq.push(['_trackEvent', 'Product_Detail', 'Tab_Select', reporting.getProductName() + '_Behind_This_Piece']); });
    $('#productDetailPage #ctl00_ContentPlaceHolderMiddle_ProductDetailTabs_behind div.video a').click(function () { _gaq.push(['_trackEvent', 'Behind_This_Piece_Video', 'Main_Play', reporting.getProductName()]); });  
    $('#productDetailPage #__tab_ctl00_ContentPlaceHolderMiddle_ProductDetailTabs_care').click(function () { _gaq.push(['_trackEvent', 'Product_Detail', 'Tab_Select', reporting.getProductName() + '_Care']); });

    // Tracking For products read more //
    $("#productDetails a.expandStory").live("click", function (event) {
        if ($(this).text() == "- Read less")
            _gaq.push(['_trackEvent', 'Product_Detail', 'Shutter', 'Care_Read_More']); 
    
    });
		       
    // Click Glossary term, Displays the definition overlay
    $('ul.gloss-letter a').click(function () {
        var term = $(this).text();
        _gaq.push(['_trackEvent', 'Glossary', 'Overlay', term]);
    });

    // Open the Furniture Care overlay image/links 
    $('ul.materials a').click(function () {_gaq.push(['_trackEvent', 'Furniture_Care', 'Overlay', $(this).parent().text()]);});

    // Click 2009 tab About Us Main
    $('ul.ui-tabs-nav li.ui-state-default ui-corner-top').click(function () { _gaq.push(['_trackEvent', 'About_Us', 'Tab_Select', 'YearTab']); });

    // Click 2010 tab Dealer Resources
    $('ul.ui-tabs-nav li.first').click(function () { _gaq.push(['_trackEvent', 'Dealer_Resources', 'Tab_Select', 'YearTab']); });

    // Click request a code 
    $('#ctl00_ContentPlaceHolderMiddle').click(function () { _gaq.push(['_trackEvent', 'Lead_Request_Quote', 'State_Prov', 'StateProvinceName']); });

    // Upon click of Follow Us on Twitter link
    $('#footer ul li ul li a[href*="twitter.com"]').click(function () {_gaq.push(['_trackEvent', 'External_Link', 'Exit_Click', 'Twitter']); });

});
