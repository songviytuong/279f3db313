﻿(function ($) {
    $(document).ready(function () {
        addToHomescreen();

        $("input").focus(function () {
            $(this).html('');
        });
        
    });
})(jQuery);

function onImgError(source, defaultCode) {
    var sku = source.src.substring(0, source.src.indexOf('_main_1')).replace('http://s7d5.scene7.com/is/image/TheodoreAlexander/', '');
    if (defaultCode != null && defaultCode != '') {
        if (sku != defaultCode) {
            source.src = source.src.replace(sku, defaultCode);
            source.onerror = "onImgError(this)";
            return true;
        }
    }

    source.src = source.src.replace(sku + '_main_1', 'ImageNotAvailable');
    // disable onerror to prevent endless loop
    source.onerror = "";
    return true;
}

function submitenter(e, btnid) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13) {
        __doPostBack(btnid, '');
        return false;
    }
    else
        return true;
}

