﻿(function ($) {

    $.fn.slidingDrawer = function (options) {

        options = $.extend({
            overlap: 20,
            speed: 500,
            reset: 1500,
            color: '#d86b2b',
            currState: 0,
            easing: 'easeOutExpo',
            click: function () { }
        }, options || {});

        return this.each(function () {

            var slider = $(this),
		 		reset;

            $('li#slider #carousel li').hover(function () {
                $(this).children('.productHover').toggle(function () {
                    $(this).fadeIn().css('zIndex', 5);

                    $('.scroll-bar-wrap').css('zIndex', 3);
                });
            }),
            //out function
			function () {
			    $(this).children('.productHover').toggle(function () {
			        $(this).fadeOut().css('zIndex', 3);
			        $('.scroll-bar-wrap').css('zIndex', 4);
			    });
			};

            $(('li#opener'), slider).hover(function () {
                // mouse over
                clearTimeout(reset);
                //
                $('#sliderKey').rotate({ maxAngle: 90, minAngle: -90,
                    bind:
						[
						{ "mouseover": function () { $(this).rotateAnimation(-90); } },
						{ "mouseout": function () { $(this).rotateAnimation(0); } }
						],
                    callback: function () {
                        //$('#sliderKeyClose').replaceWith('<img alt="key" src="/assets/images/icon-key-close.png" id="sliderKey">');	
                    }
                });
            });
            $(('li#closer'), slider).hover(function () {
                // mouse over
                clearTimeout(reset);

                $('#sliderKeyClose').rotate({ maxAngle: 90, minAngle: -90,
                    bind:
						[
						{ "mouseover": function () { $(this).rotateAnimation(90); } },
						{ "mouseout": function () { $(this).rotateAnimation(0); } }
						]

                });

            });
            $('li#opener').click(function (e) {


                $('li#closer').show();
                $('li#opener').hide();
                $('li#slider', slider).animate(
					{
					    width: 654,
					    top: 'show'


					},
					{
					    duration: options.speed,
					    easing: options.easing,
					    queue: false,
					    complete: function () { }

					})
                $('#sliderKeyClose').replaceWith('<img alt="key" src="/assets/images/icon-key-close.png" id="sliderKeyClose">');

            });
            $('li#closer').click(function (e) {
                //setCurr(this);
                currState = 0;
                //return options.click.apply(this, [e, this]);
                $('#sliderKeyClose').replaceWith('<img alt="key" src="/assets/images/icon-key.png" id="sliderKeyClose">');
                $('li#slider').animate(
					{
					    width: 0,
					    top: 'hide'

					},
					{
					    duration: options.speed,
					    easing: options.easing,
					    queue: false,
					    complete: function () {

					        //Hide the closer.
					        $('li#closer').hide();
					        //Show the opener.
					        $('li#opener').show();

					    }
					});
            });


        }); // end each

    };




})(jQuery);
  
