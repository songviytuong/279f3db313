﻿// Note this script file should only be included ONCE in each page.  Since it is already included in Main.master,
// this means any aspx page using Main.master should NOT include it anymore.
$(document).ready(function() {
    $('label.ddlHover').live('hover', function() {
        var divTwo = $('div.dropdown', $(this).parents('div[id$="divOne"]'));
        if ($('li', divTwo).length > 0) {
            // Show dropdownlist only if there are list items.
            divTwo.show();
            
            if ($.browser.msie && parseInt($.browser.version.substr(0,1)) < 8) {
                $('label.ddlHover',$('#content')).not($(this)).hide(); 
                
            }
        }
        //the following statement are for the btnGridView/btnListView dropdowns on ProductListing.aspx and Search.aspx
        if ($(this).parents('ul.paginationButtons').length > 0) {
            $('label[id$="lblSelected"]', $(this).parents('ul.paginationButtons')).removeClass('lb-over').addClass('lb-out');
        }
        $(this).removeClass('lb-out').addClass('lb-over');
    });
    $('label.dropdown:not(.ddlHover)').live('click', function() {
        var divTwo = $('div.dropdown', $(this).parents('div[id$="divOne"]'));
        if ($('li', divTwo).length > 0) {
            // Toggle dropdownlist only if there are list items.
            divTwo.toggle();
        }
        //the following statement are for the btnGridView/btnListView dropdowns on ProductListing.aspx and Search.aspx
        if ($(this).parents('ul.paginationButtons').length > 0) {
            $('label[id$="lblSelected"]', $(this).parents('ul.paginationButtons')).removeClass('lb-over').addClass('lb-out');
        }
        $(this).removeClass('lb-out').addClass('lb-over');
    });

    $('div[id$="divOne"] li.dropdownInclusive').live('mouseleave', function() {
//        console.log("mouseleave");
        $('div.dropdown', $(this)).hide();

        if ($.browser.msie && parseInt($.browser.version.substr(0,1)) < 8) {
            $('label.ddlHover',$('#content')).show(); 
        }

        if ($(this).parents('ul.paginationButtons').length == 0) {
            $('label[id$="lblSelected"]', $(this)).removeClass('lb-over').addClass('lb-out');
        }
    });

    $('ul.dropdown li').live('hover', function() {
        $(this).children('a').toggleClass('over');
    });

    $('ul.dropdown li').live('click', function() {
//        console.log("mouseleave");
        var divDropdown = $(this).parents('div.dropdown');
        divDropdown.toggle();

        //Insert selected index into hidden element for submission.
        divDropdown.next().val($('li', divDropdown).index(this));

        //Retrieve value from selected anchor innerhtml
        var userSelected = $(this).children('a').text();
        //Insert value into label to reflect user selection.
        var lblSelected = $('label[id$="lblSelected"]', $(this).parents('div[id$="divOne"]'));
        if (!lblSelected.hasClass('InitialHtmlFixed')) lblSelected.text(userSelected);

        var imgSelected = $('img[id$="imgSelected"]', $(this).parents('div[id$="divOne"]'));
        if (imgSelected != null) {
            var imgUrl = $('img', $(this)).attr('src');
            imgSelected.attr('src', imgUrl);
        }
    });
});

