﻿

$(document).ready(function() {

    /*
    $("#tabs-craft").bind("tabsshow", function(event, ui) {
    $('#content').height(($('#house-ta-content').height()) + 600);    
    });
    */

    //$('#content').height(($('#house-ta-content').height())+600);    

    $('#photos ul li').hover(
      function() {
          $(this).find('img').css('display', 'none').fadeOut('fast');
          $(this).addClass('rollover');
          $(this).find('span').fadeIn('slow').css('display', 'block');
      },
      function() {
          $(this).find('span').css('display', 'none').fadeOut('fast');
          $(this).removeClass('rollover');
          $(this).find('img').fadeIn('slow').css('display', 'block');
      }
    );
    $('#films ul li').hover(
      function() {
          $(this).addClass('rollover');
          $(this).find('span.play').fadeIn('slow').css('display', 'block');
      },
      function() {
          $(this).find('span.play').fadeOut('slow').css('display', 'none');
          $(this).removeClass('rollover');

      }
    );

    

    $('#photos ul li').click(function() {
        var image = '';
        image = $(this).find('a').attr('href');
        $(this).colorbox({ html: "<img src='" + image + "'/>", speed: 750, innerWidth: 870, innerHeight: 578, scalePhotos: true, onLoad: function() {

            $('#cboxClose').css('display', 'block');
        }
        });
    });

    $('#films ul li a, #featured-video a').colorbox({ speed: 750, innerWidth: 759, innerHeight: 426, iframe: true, scrolling: false, onLoad: function() {
        $('#cboxClose').css('display', 'none');
    }



    });

});
