﻿$.fn.centerInClient = function () {
    this.each(function (i) {
        var el = $(this);
        var x = ($(window).width() - el.width()) / 2;
        if (x <= 0) x = 5;
        var y = ($(window).height() - el.height()) / 2;
        if (y <= 0) y = 5;

        el.css("position", "absolute");
        el.css("left", x + 'px');
        el.css("top", (y + $(window).scrollTop()) + 'px');
    });
}