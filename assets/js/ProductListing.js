﻿var $images;
var $length;
var currentImage = 0;
var isPlaying = 0;
var t;

jQuery.fn.fadeToggle = function (speed, easing, callback) {
    return this.animate({ opacity: 'toggle' }, speed, easing, callback);
};

function pageLoad() {
    /** GView Overlay**/
    /** If the gridview is displayed, swap out the image on the pagination dropdowns **/
    if ($('#gridView').length > 0) {
        $('.btnGridView').find('label.dropdown').addClass('lb-over').removeClass('lb-out');
        $('.btnListView').find('label.dropdown').removeClass('lb-over').addClass('lb-out');
    } else {
        $('.btnGridView').find('label.dropdown').addClass('lb-out').removeClass('lb-over');
        $('.btnListView').find('label.dropdown').removeClass('lb-out').addClass('lb-over');
    }

    /** Open the colorbox and display gallery view**/
    $('.btnGalleryView').colorbox({ width: '980', height: '620', inline: true, href: "#galleryView", onCleanup: function () { clearTimeout(t) } });

    // In Stock Program popup
    $('.InStockHelp').colorbox({ width: '290', height: '142', inline: true, href: "#InStockHelp", onLoad: function () { $('#cboxClose').css('display', 'none'); $('#cboxOverlay').show(); $('#InStockHelp').css('display', 'block'); }, onCleanup: function () { $('#InStockHelp').css('display', 'none'); } });

    $('#cboxIPSClose').click(function () {
        $('#cboxLoadedContent').hide();
        $('#cboxOverlay').hide();
        $('#InStockHelp').css('display', 'none');
    });

    //Update categoryName and subCategoryName in FCA section on partial postback
    var categoryName = $('label[id$="ddlCategory_lblSelected"]').text();
    var ulCategory = $('#ctl00_ContentPlaceHolderMiddle_ddlCategory_list');
    var selectedIndex = $('#ctl00_ContentPlaceHolderMiddle_ddlCategory_hidSelectedIndex').val();
    if (selectedIndex != '') {
        var categoryId = $('a:eq(' + selectedIndex + ')', ulCategory).attr('dbid');
        $('a[id$="lnkParent"]').attr('href', "/CategoryListing.aspx?categoryid=" + categoryId);
    }

    var subCategoryName = $('label[id$="ddlSubcategory_lblSelected"]').text();

    $.fn.vAlign = function() {
        return this.each(function(i) {
            $(this).parent().css('display', 'block');
            var ah = $(this).height();
            var ph = $(this).parent().height();
            var mh = (ph - ah) / 2;

            $(this).css('margin-top', (mh - 8));
            $(this).parent().css('display', 'none');
        });
    };
    $('#gridView .productname h1').vAlign();
}

$(document).bind('cbox_open', function() {
    $('div.galleryDetails').mouseenter(function() {
        $('ul.gpInfo').fadeTo('fast',1);
    });
    $('div.galleryDetails').mouseleave(function() {
        $('ul.gpInfo').fadeTo('fast',0);
    });
});

$(document).ready(function () {
    $('#galleryView .btnPrevious').live('hover', function () {
        $(this).toggleClass('btnPreviousOver');
    });
    $('#galleryView .btnPlay').live('hover', function () {
        $(this).toggleClass('btnPlayOver');

    });
    $('#galleryView .btnNext').live('hover', function () {
        $(this).toggleClass('btnNextOver');
    });


    $('#gridView li').live('hover', function () {
        $(this).children('.productname').fadeToggle();
    });

    $('#preButton input').live('hover', function () {
        $(this).css('backgroundPosition', '-65px -38px');
    });

    /** d) : Optional Close Pop up window feature**/
    $('.iconClose').live('click', function (event) {
        event.preventDefault();
        $('#fuzz').slideUp('fast', function () {
            //Show the appropriate Div...
            $('div.popup-shadow').hide();
        });
    });

    $('#LookBook').live('hover', function () {
        $(this).show();
    });

    $("#nextButton").live('click', function (event) {
        if (++currentImage > $length - 1) currentImage = 0;
        DisplayImage();
        event.preventDefault();
    });

    $("#prevButton").live('click', function (event) {
        if (--currentImage < 0) currentImage = $length - 1;
        DisplayImage();
        event.preventDefault();
    });

    $("#playButton").live('click', function (event) {
        playButtonClick();
    });

    $('ul[id$="ddlCategory_list"] a').live('click', function (event) {
        //var categoryId = $(this).attr('dbid');
        $('#fuzz').slideDown('fast');
        //window.location.replace("/ProductListing.aspx?categoryId=" + categoryId);
    });

    $('ul[id$="ddlSubcategory_list"] a').live('click', function (event) {
        //var typeId = $(this).attr('dbid');
        $('#fuzz').slideDown('fast');
        //window.location.replace("/ProductListing.aspx?typeId=" + typeId);
    });

});

function GetImages() {
    _gaq.push(['_trackEvent', 'Product_Listing', 'View', 'Gallery']);
    var ulCategory = $('#ctl00_ContentPlaceHolderMiddle_ddlCategory_list');
    var ulCategorySelectedIndex = $('#ctl00_ContentPlaceHolderMiddle_ddlCategory_hidSelectedIndex').val();
    var strCategoryId = $('a:eq(' + ulCategorySelectedIndex + ')', ulCategory).attr('dbid');

    var ulSubCategory = $('#ctl00_ContentPlaceHolderMiddle_ddlSubcategory_list');
    var ulSubCategorySelectedIndex = $('#ctl00_ContentPlaceHolderMiddle_ddlSubcategory_hidSelectedIndex').val();
    var strTypeId = $('a:eq(' + ulSubCategorySelectedIndex + ')', ulSubCategory).attr('dbid');

    var newOnly = $('input:checkbox[id$="cbNewest"]').attr('checked');
    if (newOnly == null) newOnly = false;
    var inStock = $('input:checkbox[id$="cbInStock"]').attr('checked');
    if (inStock == null) inStock = false;

    var queryStrings = GetQueryStrings();
    var strFilter = '';
    var strFilterId = '';
    if (queryStrings['collectionid'] != null && queryStrings['collectionid'].trim().length > 0) {
        strFilter = 'collectionid';
        strFilterId = queryStrings['collectionid'];
    } else if (queryStrings['themeid'] != null && queryStrings['themeid'].trim().length > 0) {
        strFilter = 'themeid';
        strFilterId = queryStrings['themeid'];
    } else if (queryStrings['styleid'] != null && queryStrings['styleid'].trim().length > 0) {
        strFilter = 'styleid';
        strFilterId = queryStrings['styleid'];
    } else if (queryStrings['roomid'] != null && queryStrings['roomid'].trim().length > 0) {
        strFilter = 'roomid';
        strFilterId = queryStrings['roomid'];    
    }

    PageMethods.GetImagesList(strCategoryId, strTypeId, strFilter, strFilterId, newOnly, inStock, onGetImagesSuccess, onGetImagesFailure);
}
function onGetImagesSuccess(result) {
    $images = result.split("|");
    $length = $images.length;
    currentImage = 0;
    $("#lblTotal").html($length);
    isPlaying = 0;
    playButtonClick();
}

function onGetImagesFailure(res) {
    alert(res.get_message());
}

function playButtonClick() {
    if (!isPlaying) {
        isPlaying = true;
        //$("#playButton").val("Pause");
        switchImage();
    } else {
        isPlaying = false;
        //$("#playButton").val("Play");
        clearTimeout(t);
    }
}

function switchImage() {
    DisplayImage();
    if (++currentImage > $length - 1) currentImage = 0;
    t = setTimeout("switchImage()", 3000);
}

function DisplayImage() {
    $("#imgGalleryView").attr("src", getPartOfString($images[currentImage], "URL"));
    $("#collection").html(getPartOfString($images[currentImage], "Collection"));
    $("#aItemURL").attr("href", "/ProductDetail.aspx?ItemID=" + getPartOfString($images[currentImage], "ID"));
    $("#lblName").html(getPartOfString($images[currentImage], "ProductName")).attr("title", getPartOfString($images[currentImage], "ProductName"));
    $("#itemNo").html(currentImage + 1);
}

function getPartOfString(stringName, partName) {
    var itemInfo = stringName.split(";");
    switch (partName) {
        case "URL":
            return itemInfo[0];
        case "ProductName":
            return itemInfo[1];
        case "ID":
            return itemInfo[2];
        case "Collection":
            return itemInfo[3];
        default:
            return itemInfo[1];
    }
}

function GetQueryStrings() {
    // get querystring as an array split on "&"
    var querystring = location.search.replace('?', '').split('&');
    // declare object
    var queryObj = {};
    // loop through each name-value pair and populate object
    for (var i = 0; i < querystring.length; i++) {
        // get name and value
        var name = querystring[i].split('=')[0].toLowerCase();
        var value = querystring[i].split('=')[1];
        // populate object
        queryObj[name] = value;
    }
    return queryObj;
}
/*(function($) {
   
    $.fn.vAlign = function() {
        return this.each(function(i) {
            $(this).parent().css('display', 'block');
            var ah = $(this).height();
            var ph = $(this).parent().height();
            var mh = (ph - ah) / 2;

            $(this).css('margin-top', (mh - 8));
            $(this).parent().css('display', 'none');
        });
    };
})(jQuery);


$(document).ready(function() {

    $('#gridView .productname h1').vAlign();
    

});*/





