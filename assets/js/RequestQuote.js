﻿$(document).ready(function () {
    $('input[name$="rblDesignerRequestQuote"]').live("change", function () {
        if ($("input[name$='rblDesignerRequestQuote']:checked").val() == 'Yes')
            $('#designerDiv').show();
        else if ($("input[name$='rblDesignerRequestQuote']:checked").val() == 'No')
            $('#designerDiv').hide();
    });
});

function ValidateRequestQuoteControls() {
    var isValidated = true;
    $('#pValidationError').html('<label>Please correct the highlighted fields before continuing:</label><ul></ul>');
    isValidated &= ValidateRequiredField($('input[id$="tbNameRequestQuote"]'), 'Please Enter Your Name', 'pValidationError');
    isValidated &= ValidateRequiredField($('input[id$="tbPhoneRequestQuote"]'), 'Daytime Phone:  Enter your Daytime Phone', 'pValidationError');
    isValidated &= ValidatePhoneNumber($('input[id$="tbPhoneRequestQuote"]'), 'pValidationError');
    isValidated &= ValidateRequiredField($('input[id$="tbEmailRequestQuote"]'), 'Email Address: Enter Your Email Address', 'pValidationError');
    isValidated &= ValidateEmail($('input[id$="tbEmailRequestQuote"]'), 'pValidationError');
    isValidated &= ValidateRequiredField($('input[id$="tbCityRequestQuote"]'), 'City:  Enter a City', 'pValidationError');
    //isValidated &= ValidateRequiredField($('input[id$="tbState"]'), 'State/Province:  Enter your State/Province');
    isValidated &= ValidateRequiredField($('input[id$="tbCountryRequestQuote"]'), 'Country:  Enter a Country', 'pValidationError');
    //isValidated &= ValidateRequiredField($('input[id$="tbZipCode"]'), 'Zip Code:  Enter a Zip Code');

    if (!isValidated) {
        _gaq.push(['_trackEvent','Lead_Request_Quote','Error','All_Fields']);
        $('#pValidationError').show();
    }
    else {
        var productName = $.trim($('h3#productTitle').text());
		_gaq.push(['_trackEvent','Lead_Request_Quote','City',$('#requestQuate input.city[id$="tbCityRequestQuote"]').val()]);
		_gaq.push(['_trackEvent','Lead_Request_Quote','State_Prov',$('#requestQuate input.state[id$="tbStateRequestQuote"]').val()]);
		_gaq.push(['_trackEvent','Lead_Request_Quote','Country',$('#requestQuate input.country[id$="tbCountryRequestQuote"]').val()]);
		_gaq.push(['_trackEvent','Lead_Request_Quote','Success',$('#productInfo h2#productTitle span').text()]);

        $('#pValidationError').hide();
    }
    return isValidated;
}

function CloseRequestQuoteOverlay() {
    $('#pValidationError').hide();
    $('#designerDiv').hide();    
    $('#fuzz').slideUp('fast', function () {
        $('#content, #lookLinks, #signInLinks').css('visibility', 'visible');
        $('#requestQuate').hide();
    });

    $('input[id$="tbNameRequestQuote"]').val("");
    $('input[id$="tbNameRequestQuote"]').removeClass("highlight");
    $('input[id$="tbPhoneRequestQuote"]').val("");
    $('input[id$="tbPhoneRequestQuote"]').removeClass("highlight");
    $('input[id$="tbEmailRequestQuote"]').val("");
    $('input[id$="tbEmailRequestQuote"]').removeClass("highlight");
    $('input[id$="tbCityRequestQuote"]').val("");
    $('input[id$="tbCityRequestQuote"]').removeClass("highlight");
    $('input[id$="tbCountryRequestQuote"]').val("");
    $('input[id$="tbCountryRequestQuote"]').removeClass("highlight");
    $('input[id$="tbAddressRequestQuote"]').val("");
    $('input[id$="tbStateRequestQuote"]').val("");
    $('input[id$="tbZipCodeRequestQuote"]').val("");
    $('.idleField').val('');
    $('input[name$="rblContactRequestQuote"]').attr('checked', 'true');
    $('input[name$="rblDesignerRequestQuote"]:eq(0)').attr('checked', 'checked');
    $('input[name$="rblFromRequestQuote"]').attr('checked', "");
    $('input[id$="chkCopyYouRequestQuote"]').attr('checked', "");
}
