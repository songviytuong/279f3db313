﻿$(document).ready(function () {
    $("#tabs").tabs();

    $('a.linkSignin').live("click", function (event) {
        $('#fuzz').slideDown(100).css('zIndex', '4').height($(document).height());
        if ($.browser.msie && $.browser.version <= 7.0) {
            $('.hideformenu').css("visibility", "hidden")
        }
        $('#divSignIn').show();
        $('#ctl00_ctl00_tbUsername').focus();
        event.preventDefault();
    });

    $('#btnClose, #btnCancel').live("click", function (event) {
        var divSignIn = $('#divSignIn');
        $('input[id$="tbUsername"]', divSignIn).val("");
        $('input[id$="tbUsername"]', divSignIn).removeClass("highlight");
        $('input[id$="tbPassword"]', divSignIn).val("");
        $('input[id$="tbPassword"]', divSignIn).removeClass("highlight");
        $('span[id$="lblError"]', divSignIn).text("");
        $('#pValidationSignInError').html('');
        $('#pValidationSignInError').hide();
        if ($.browser.msie && $.browser.version <= 7.0) {
            $('.hideformenu').css("visibility", "visible")
        }
        $('#divSignIn').hide();
        $('#fuzz').slideUp(100).css('zIndex', '2');
        $('#menu, #lookLinks').css('visibility', 'visible');
    });

    // When 'X' or 'Cancel' button clicked on Overlays
    $('a.btnClose, a.backLink, a.ClosePopup, a.popupClose').live("click", function (event) {
        var divToClose = $(this).parents('div.overlay');
        divToClose.hide();
        //$('#fuzz').slideUp(100); ; //.css('zIndex', '2');
        $('#fuzz').hide();//.css('zIndex', '2');
        $('#searchPanel, #advancedSearch, #lookLinks, #signInLinks, #menu').css('visibility', 'visible');
        event.preventDefault();
    });

    



/*    $('ul.dropdown[id$="ddlLookBook_list"] li.lastItem a').live('click', function(e) {
        $('#divSelectOrCreateLB').show().centerInClient();
        e.preventDefault();
    });*/
    $('ul.dropdown[id$="ddlLookBook_list"] li.lastItem a').live('click', function(e) {
    $('#fuzz').slideDown('fast', function() {
        $('#fuzz').height($(document).height());
        $('#divSelectOrCreateLB').show().centerInClient();
        e.preventDefault();
    });
    
    
    

    });    

    $('#divExistingLBs li').live("hover", function () {
        $(this).toggleClass('over');
    });
});

function ValidateSignIn() {
    var isValidated = true;
    $('#pValidationSignInError').html('<ul></ul>');
    isValidated &= ValidateRequiredField($('#divSignIn input[id$="tbUsername"]'), 'Please enter username', 'pValidationSignInError');
    isValidated &= ValidateRequiredField($('#divSignIn input[id$="tbPassword"]'), 'Please enter password', 'pValidationSignInError');

    if (!isValidated)
        $('#pValidationSignInError').show();
    else
        $('#pValidationSignInError').hide();
    return isValidated;
}

