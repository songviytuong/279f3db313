<?php
$lang['friendlyname'] = 'Editeur de syntaxe color&eacute; CodeMirror';
$lang['postinstall'] = 'Le module CodeMirror a &eacute;t&eacute; install&eacute; avec succ&egrave;s';
$lang['needpermission'] = '&#039;Vous avez besoin de l&#039;autorisation &#039;%s&#039;  pour cette fonction';
$lang['settingstab'] = 'Param&egrave;tres';
$lang['savesettings'] = 'Sauvegarder les param&egrave;tres';
$lang['settingssaved'] = 'Param&egrave;tres sauvegard&eacute;s';
$lang['htmltest'] = 'Test de syntaxe HTML ';
$lang['csstest'] = 'Test de syntaxe CSS ';
$lang['phptest'] = 'Test de syntaxe PHP';
$lang['jstest'] = 'Test de syntaxe JavaScript';
$lang['showlinenumbers'] = 'Afficher le nombre de lignes';
$lang['allowfullscreen'] = 'Autoriser le mode plein &eacute;cran (F11)';
$lang['textwrapping'] = 'Renvoi &agrave; la ligne automatique ';
$lang['automatchparens'] = 'Afficher les correspondances de parenth&egrave;ses';
$lang['tabhandling'] = 'Comment g&eacute;rer le clic sur la touche TAB';
$lang['tabdefault'] = 'Laisser le navigateur d&eacute;cider';
$lang['tabindent'] = 'Indenter';
$lang['tabspaces'] = 'Ins&eacute;rer des espaces';
$lang['tabshift'] = 'Shift vers la droite, shift-TAB shifts vers la gauche';
$lang['tabsize'] = 'Taille des onglets';
$lang['cssadditions'] = 'Ajouts &agrave; la feuille de style de CodeMirror.';
$lang['theme'] = 'Th&egrave;me';
$lang['help'] = '<strong>Que fait ce module&nbsp;?</strong>
<br/>
Le module CodeMirror vous permet de modifier le contenu, les gabarits et les feuilles de style en utilisant un &eacute;diteur de syntaxe color&eacute; l&#039;int&eacute;rieur de votre navigateur. 
Il peut aussi bien remplacer l&#039;&eacute;diteur WYSIWYG, et permettre de visualiser-&eacute;diter les gabarits, feuilles de style. (Uniquement pour version CMSMS 1.1 ou plus)
<br/><br/>
<strong>Comment l&#039;utiliser&nbsp;?</strong><br/>
Il vous suffit de l&#039;installer, et dans &quot;Mes pr&eacute;f&eacute;rences / Pr&eacute;f&eacute;rences de l&#039;utilisateur&quot; choisir avec le menu d&eacute;roulant CodeMirror dans &quot;S&eacute;lectionner la syntaxe surlign&eacute;e&quot; et/ou &eacute;ventuellement dans &quot;S&eacute;lection du WYSIWYG &agrave; utiliser&quot;.
';
?>