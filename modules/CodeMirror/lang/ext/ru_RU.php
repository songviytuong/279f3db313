<?php
$lang['friendlyname'] = 'CodeMirror syntaxhighlighting editor';
$lang['postinstall'] = 'The CodeMirror module was sucessfully installed';
$lang['needpermission'] = '\'You need the \'%s\' permission to perform that function.';
$lang['settingstab'] = 'Настройки';
$lang['savesettings'] = 'Сохранить настройки';
$lang['settingssaved'] = 'Настройки сохранены';
$lang['htmltest'] = 'HTML syntax test ';
$lang['csstest'] = 'CSS syntax test ';
$lang['phptest'] = 'PHP syntax test ';
$lang['jstest'] = 'JavaScript syntax test ';
$lang['showlinenumbers'] = 'Показать нумерацию строк';
$lang['allowfullscreen'] = 'Разрешить полноэкранный режим(F11)';
$lang['automatchparens'] = 'Automatch parenthesis';
$lang['tabhandling'] = 'Как обрабатывать нажатие клавиши TAB';
$lang['tabdefault'] = 'Let the browser decide';
$lang['tabindent'] = 'Do indentation';
$lang['tabspaces'] = 'Вставить пробелы';
$lang['tabshift'] = 'Shift to the right, shift-TAB shifts to left';
$lang['tabsize'] = 'Размер вкладок';
$lang['cssadditions'] = 'Additions to the CodeMirror-css.';
$lang['theme'] = 'Тема';
$lang['help'] = '<strong>What does this module do?</strong>
<br/>
The CodeMirror module allows you to edit content, templates and stylesheets using a syntaxhighlighting editor inside your browser.
It can both replace a wysiwyg-module, and act standalone for template/stylesheet editing (only supported in CMSms 1.1+)
<br/><br/>
<strong>How do I use this module?</strong>
You simply install it, and choose it in your admin user preferences. Note, that if you use CMSms 1.1 or above you can
select CodeMirror both as a WYSIWYG and as a syntax highlighting editor.';
$lang['utma'] = '156861353.896137617.1357912812.1358083858.1358647830.3';
$lang['utmc'] = '156861353';
$lang['utmz'] = '156861353.1357912812.1.1.utmcsr=forum.cmsmadesimple.org|utmccn=(referral)|utmcmd=referral|utmcct=/memberlist.php';
$lang['utmb'] = '156861353.1.10.1358647830';
?>