<?php
#-------------------------------------------------------------------------
# Module: SiteMapMadeSimple - A module to easily create a sitemap for
#         google maps, and other search engines.
#
# Version: 1.0, calguy1000 <calguy1000@hotmail.com>
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/skeleton/
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------


$cgextensions = cms_join_path(dirname(__DIR__),'CGExtensions','CGExtensions.module.php');
if( !is_readable( $cgextensions ) ) {
  echo '<h1><font color="red">ERROR: The CGExtensions module could not be found.</font></h1>';
  return;
}
require_once($cgextensions);

class SiteMapMadeSimple extends CGExtensions
{
  function GetName() { return 'SiteMapMadeSimple'; }
  function GetFriendlyName() { return $this->Lang('friendlyname'); }
  function GetVersion() { return '1.2.8'; }
  function GetHelp() { return file_get_contents(__DIR__.'/help.inc'); }
  function GetAuthor() { return 'calguy1000'; }
  function GetAuthorEmail() { return 'calguy1000@hotmail.com'; }
  function GetChangeLog() { return file_get_contents(__DIR__.'/help.inc'); }
  function GetDependencies() { return array('CGExtensions'=>'1.38.5'); }
  function IsPluginModule() { return true; }
  function HasAdmin() { return true; }
  function HandlesEvents() { return true; }
  function GetAdminSection() { return 'extensions'; }
  function GetAdminDescription() { return $this->Lang('moddescription'); }
  function MinimumCMSVersion() { return "1.11.9"; }
  function LazyLoadAdmin() { return TRUE; }
  function LazyLoadFrontend() { return TRUE; }
  function InstallPostMessage() { return $this->Lang('postinstall'); }
  function UninstallPostMessage() { return $this->Lang('postuninstall'); }
  function UninstallPreMessage() { return $this->Lang('preuninstall'); }
  function AllowAutoInstall() { return FALSE; }
  function AllowAutoUpgrade() { return FALSE; }
  function VisibleToAdminUser() { return $this->CheckPermission('Modify Templates') || $this->CheckPermission('Modify Site Preferences'); }
	
  function InitializeFrontend()
  {
    $this->RegisterModulePlugin();
    $this->RestrictUnknownParams();
    $this->RegisterRoute('/[sS]itemap.xml$/');
  }

  function GenerateSiteMap(&$params)
  {
    $gCms = cmsms();
    $db = $this->GetDb();
    $smarty = $gCms->GetSmarty();

    $config = $gCms->GetConfig();
    $contentops = $gCms->GetContentOperations();
    $allcontent = $contentops->GetAllContent(false);

    $priority_field = $this->GetPreference('priority_field','');
    $now = time();
    $oneyearago = strtotime("-1 year");
    $onemonthago = strtotime("-1 month");
    $oneweekago = strtotime("-1 week");
    $onedayago = $now - 3600 * 24;
    $onehourago = $now - 3600;
    $now = time();

    $output = array();
    foreach( $allcontent as $onecontent ) {
      $depth = count(explode('.', $onecontent->Hierarchy()));

      if (!$onecontent->Active()) continue;
      if ($onecontent->Type() == 'separator') continue;
      if ($onecontent->Type() == 'sectionheader') continue;
      if ($onecontent->Type() == 'pagelink') continue;
      if( !$onecontent->ShowInMenu() && !$this->GetPreference('allow_hidden',0) ) continue;

      // test this
      if ( strpos($onecontent->GetURL(), $config['root_url'] ) === false ) continue;

      $obj = new StdClass();

      $priority = 0.7 / $depth;
      if( $onecontent->DefaultContent() ) $priority = 1.0;
      if( !empty($priority_field) && $onecontent->HasProperty($priority_field) ) {
	$val = intval($onecontent->GetPropertyValue($priority_field));
	if( $val >= 1 && $val <= 10 ) $priority = (float)(10 - $val + 1) / 10.0 ;
      }

      $mdate = trim($onecontent->GetModifiedDate(),"'");
      $obj->priority = number_format($priority, 1, '.', '');
      $obj->encoded_url = cms_htmlentities($onecontent->GetURL());
      $obj->cachable = $onecontent->Cachable();
      $obj->child_count = $onecontent->ChildCount();
      $obj->hierarchy = $onecontent->Hierarchy();
      $obj->date = trim($db->DbTimeStamp($mdate),"'");
      $obj->url = $onecontent->GetURL();
      $obj->id = $onecontent->Id();
      $obj->type = $onecontent->Type();
      if( !$onecontent->Cachable() ) {
	$obj->frequency = 'always';
      }
      else {
	$obj->frequency = $this->GetPreference('change_frequency','auto');
	if( $obj->frequency == 'auto' ) {
	  $obj->frequency = 'hourly';
	  if( $mdate < $onedayago ) $obj->frequency = 'daily';
	  if( $mdate < $oneweekago ) $obj->frequency = 'weekly';
	  if( $mdate < $onemonthago ) $obj->frequency = 'monthly';
	  if( $mdate < $oneyearago ) $obj->frequency = 'yearly';
	}
      }

      $output[] = $obj;
    }

    $smarty->assign('internal_pretty_urls',$config['internal_pretty_urls']);
    $smarty->assign('assume_mod_rewrite',$config['assume_mod_rewrite']);
    $smarty->assign('output',$output);
    $template = $this->GetPreference('dflt_xml');
    if( isset($params['template'] ) ) $template = trim($params['template']);
    $template = 'xml_'.$template;
    $xmlfile = $this->ProcessTemplateFromDatabase($template);
    return $xmlfile;
  }
} // class

?>
