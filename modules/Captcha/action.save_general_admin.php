<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission('Modify Site Preferences') ) return;
$this->errors = array_merge($this->errors, \Captcha\utils::get_GD_errors() );
$plugins = $this->getAvailableLibs();
$active_lib_current = $this->GetPreference('active_lib');
$active_lib = isset($params['select_captchalib']) ? $params['select_captchalib'] : '';
$enable_pear = isset($params['enable_pear']);
$this->SetPreference('enable_pear', $enable_pear);
$clear_cache = isset($params['clear_cache']);
$this->errors = array_merge($this->errors, \Captcha\utils::get_GD_errors() );

  
// clear cached captcha images if checkbox was selected
if($clear_cache)
{
  $active_lib_obj = $this->getActiveLib();
  $image_path = $active_lib_obj->getImagePath();
  
  $image_filenames = $this->getImageFilenames($image_path);
  
  $deleted = 0;
  foreach ($image_filenames as $filename)
  {
    if (unlink(cms_join_path($image_path, $filename)))
    {
      $deleted++;
    }
  }
  
  if ($deleted > 0) {
    $msg = 
      ($deleted == 1)
      ? $this->Lang('msg_deleted_cache_single')
      : $this->Lang('msg_deleted_cache', $deleted);
    $changes = TRUE;
    $this->addMessage($msg);
  }
}


if( !empty($active_lib) && $active_lib != $active_lib_current )
{

  if($active_lib == 'pear')
  {     
    if(!$enable_pear)
    {
      $this->addError($this->Lang('msg_pear_disable_while_selected'));
      $params = array('active_tab' => 'general');
      $this->SetMessage( $this->getMessages() );
      $this->SetError( $this->getErrors() ); 
      $this->Redirect($id, 'defaultadmin', $returnid, $params);
    }
    
    \Captcha\plugins_ops::load_plugin($active_lib);
    $tmp = \Captcha\plugins_ops::get_loaded();
    $plugin = $tmp[$active_lib]; 

    if( !$plugin->checkAvailability() )
    {
      $this->addError(
                        $this->lang('msg_pear_unavailable')
                                    . '<br />' 
                                    . $this->Lang('msg_pear_disabled')
                      );
      
      $params = array('active_tab' => 'general');
      $this->Redirect($id, 'defaultadmin', $returnid, $params);
    }
    
    // set the pear library object is_available property
    $plugin->setIsAvailable($enable_pear);
    
    // set the pear library object enabled property
    $plugin->setIsEnabled($enable_pear);
    
    // show a message to the user
    $msg = $enable_pear ? $this->Lang('msg_pear_enabled') : $this->Lang('msg_pear_disabled');
    $this->addMessage($msg);
  }
  
  // set the active_lib preference
  $this->setActiveLib($plugins[$active_lib]);
  
  $this->addMessage( $this->Lang('msg_active_lib_changed', $plugins[$active_lib]->getFriendlyName() ) );
}

$this->SetMessage( $this->getMessages() );
$this->SetError( $this->getErrors() ); 

$params = array('active_tab' => 'general');
$this->Redirect($id, 'defaultadmin', $returnid, $params);
?>