<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------

// Please type in all needed values before run the script!

    require_once("./hn_captcha.class.php");

    // ConfigArray
    $CAPTCHA_INIT = array(

            // string: absolute path (with trailing slash!) to a php-writeable tempfolder which is also accessible via HTTP!
            'tempfolder'     => $_SERVER['DOCUMENT_ROOT'].'/_tmp/',

            // string: absolute path (in filesystem, with trailing slash!) to folder which contain your TrueType-Fontfiles.
            'TTF_folder'     => dirname(__FILE__).'/fonts/',

            // mixed (array or string): basename(s) of TrueType-Fontfiles, OR the string 'AUTO'. AUTO scanns the TTF_folder for files ending with '.ttf' and include them in an Array.
            // Attention, the names have to be written casesensitive!
            //'TTF_RANGE'    => 'NewRoman.ttf',
            //'TTF_RANGE'    => 'AUTO',
            //'TTF_RANGE'    => array('actionj.ttf','bboron.ttf','epilog.ttf','fresnel.ttf','lexo.ttf','tetanus.ttf','thisprty.ttf','tomnr.ttf'),
            'TTF_RANGE'    => 'AUTO',

            'chars'          => 5,       // integer: number of chars to use for ID
            'minsize'        => 20,      // integer: minimal size of chars
            'maxsize'        => 30,      // integer: maximal size of chars
            'maxrotation'    => 25,      // integer: define the maximal angle for char-rotation, good results are between 0 and 30
            'use_only_md5'   => FALSE,   // boolean: use chars from 0-9 and A-F, or 0-9 and A-Z

            'noise'          => TRUE,    // boolean: TRUE = noisy chars | FALSE = grid
            'websafecolors'  => FALSE,   // boolean
            'refreshlink'    => TRUE,    // boolean
            'lang'           => 'en',    // string:  ['en'|'de'|'fr'|'it'|'fi']
            'maxtry'         => 3,       // integer: [1-9]

            'badguys_url'    => '/',     // string: URL
            'secretstring'   => 'A very, very secret string which is used to generate a md5-key!',
            'secretposition' => 9        // integer: [1-32]
    );

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<TITLE>PHP-Captcha-Class :: DEMO</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">

<style type="text/css">
<!--

/*********************************
 *
 *    globale HTML Styles
 *
 */
    a:link
    {
        color: #0079C5;
        background: transparent;
        text-decoration: none;
    }
    a:visited
    {
        color: #5DA3ED;
        background: transparent;
        text-decoration: none;
    }
    a:hover,
    a:active,
    a:focus
    {
        color: #cd3021;
        background: transparent;
        text-decoration: underline;
    }

    html,
    body
    {
        margin-top: 20px;
        margin-bottom: 20px;
        margin-left: 20px;
        margin-right: 20px;
        padding-top: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        padding-right: 0px;
    }

    body
    {
        background-color: #FFFFFF;
        color: #000000;
        font-family: Verdana, Helvetica, Arial, sans-serif;
    }

    h3
    {
        margin-left: 30px;
        margin-right: 20px;
        background: transparent;
        color: #222222;
        font-size: 20px;
        font-style: normal;
        font-weight: bold;
        font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
        line-height: 100%;
        letter-spacing: 1px;
    }

/*********************************
 *
 *    CAPTCHA-Styles
 *
 */
    p.captcha_1,
    p.captcha_2,
    p.captcha_notvalid
    {
        margin-left: 30px;
        margin-right: 20px;
        font-size: 12px;
        font-style: normal;
        font-weight: normal;
        font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
        background: transparent;
        color: #000000;
    }
    p.captcha_2
    {
        font-size: 10px;
        font-style: italic;
        font-weight: normal;
    }
    p.captcha_notvalid
    {
        font-weight: bold;
        color: #FFAAAA;
    }

    .captchapict
    {
        margin: 0px 0px 0px 0px;
        padding: 0px 0px 0px 0px;
        border-style: inset;
        border-width: 4px;
        border-color: #C0C0C0;
    }

    #captcha
    {
        margin-left: 30px;
        margin-right: 30px;
        border-style: dashed;
        border-width: 2px;
        border-color: #FFD940;
    }
-->
</style>
</head>
<body>
<h3>This is a demo of hn_captcha.class.php with a Standalone-Form:</h3>

<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------

    $captcha = new hn_captcha($CAPTCHA_INIT, TRUE);
    //$captcha =& new hn_captcha($CAPTCHA_INIT);

    switch($captcha->validate_submit())
    {

        // was submitted and has valid keys
        case 1:
            // PUT IN ALL YOUR STUFF HERE //
                    echo "<p><br>Congratulation. You will get the resource now.";
                    echo "<br><br><a href=\"".$_SERVER['PHP_SELF']."?download=yes&id=1234\">New DEMO</a></p>";
            break;


        // was submitted with no matching keys, but has not reached the maximum try's
        case 2:
            echo $captcha->display_form();
            break;


        // was submitted, has bad keys and also reached the maximum try's
        case 3:
            //if(!headers_sent() && isset($captcha->badguys_url)) header('location: '.$captcha->badguys_url);
                    echo "<p><br>Reached the maximum try's of ".$captcha->maxtry." without success!";
                    echo "<br><br><a href=\"".$_SERVER['PHP_SELF']."?download=yes&id=1234\">New DEMO</a></p>";
            break;


        // was not submitted, first entry
        default:
            echo $captcha->display_form();
            break;

    }

?>

</body>
</html>
