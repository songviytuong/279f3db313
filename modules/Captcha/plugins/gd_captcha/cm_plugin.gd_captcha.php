<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
/**
 * GD captcha library
 * @see class.captchalib.php
 * @see CaptchaLib
 * @package Captcha
*/
namespace Captcha;

/**
 * @extends \Captcha\cm_plugin_base
*/
class gd_captcha extends \Captcha\cm_plugin_base
{

	function __construct()
	{
		parent::__construct();  
    $this->setImageUrl($this->cmsconfig['root_url'] . '/modules/Captcha/plugins/gd_captcha/lib/captcha.php');
    $this->setFriendlyName('GD Captcha');
	}
	
	/**
	* Sets the session variable and returns the HTML to show the captcha
	*/
	function getCaptcha($options = array()) 
	{
		if ( !session_id() ) session_start();
		return ('<img alt="" src="' . $this->getImageUrl() . '?sess=' . session_name(session_name()) . '" />');
	}
  
  function checkAvailability() 
  {
    $ret = function_exists('gd_info') && is_readable( $this->getPath() );
    $this->setIsAvailable($ret);
    return $ret;
  }
	
	function checkCaptcha($input, $options = array())
	{
    return $_SESSION['session_captchaText'] == $input ? TRUE : FALSE;
	}

	function load()
	{
	}
  
  function NeedsInputField()
  {
    return TRUE;
  }   
}
?>