<?php

final class products_timelines implements ArrayAccess
{
    private static $_keys = array('id', 'ids',
        'product_id',
        'title_en',
        'title_my',
        'description_en',
        'description_my',
        'meals',
        'vehicle',
        'iorder');
    private $_data = array('id' => '', 'ids' => '',
        'product_id' => '',
        'title_en' => '',
        'title_my' => '',
        'description_en' => '',
        'description_my' => '',
        'meals' => '',
        'vehicle' => '',
        'iorder' => -1);
    
    private $_dirty;

    public function __clone()
    {
        $this->_dirty = TRUE;
        $this->_data['id'] = '';
        $this->_data['iorder'] = -1;
    }

    public function offsetGet($key) {
        if (!in_array($key, self::$_keys)) {
            throw new CmsException('Invalid key ' . $key . ' specified for products_timelines object');
        }
        if (isset($this->_data[$key]))
            return $this->_data[$key];
    }

    public function offsetSet($key, $value) {
        if (!in_array($key, self::$_keys)) {
            throw new CmsException('Invalid key ' . $key . ' specified for products_timelines object');
        }

        switch ($key) {
            case 'id':
                throw new CmsException('id cannot be set for the products_timelines object');
                break;

            case 'iorder':
                throw new CmsException('iorder cannot be set for the products_timelines object in this manner');
                break;

            case 'product_id':
                $value = (int) $value;
                if ($value < 1)
                    throw new CmsException('product id cannot be negative or zero for products_timelines object');
                break;

            case 'title_en':break;
            case 'title_my':break;
            case 'description_en':break;
            case 'description_my':break;
            case 'ids':break;
            case 'meals':break;
            case 'vehicle':break;

                $this->_data[$key] = (string) $value;
                $this->_dirty = TRUE;
        }
    }

    public function offsetUnset($key) {
        if (!in_array($key, self::$_keys)) {
            throw new CmsException('Invalid key ' . $key . ' specified for products_timelines object');
        }

        switch ($key) {
            case 'id':
            case 'ioroder':
                throw new CmsException("$key cannot be unset for a " . __CLASS__ . ' object');
            default:
                unset($this->_data[$key]);
                $this->_dirty = TRUE;
                break;
        }
    }

    public function offsetExists($key) {
        if (!in_array($key, self::$_keys)) {
            throw new CmsException('Invalid key ' . $key . ' specified for products_timelines object');
        }

        return TRUE;
    }

    private function _insert() {
        global $mleblock;
        $db = cmsms()->GetDb();
        if ($this->_data['iorder'] < 1) {
            // calculate a new iorder for this product
            $query = 'SELECT MAX(COALESCE(iorder,0))+1 FROM ' . cms_db_prefix() . 'module_products_timelines WHERE product_id = ?';
            $this->_data['iorder'] = $db->GetOne($query, array($this->_data['product_id']));
        }
        $data = $this->_data;
        if ($data['title' . $mleblock] != '') {
            $query = 'INSERT INTO ' . cms_db_prefix() . 'module_products_timelines
                  (product_id,title' . $mleblock . ',description' . $mleblock . ',meals,vehicle,iorder)
                  VALUES (?,?,?,?,?,?)';
            $dbr = $db->Execute($query, array($data['product_id'], $data['title' . $mleblock], $data['description' . $mleblock], $data["meals"], $data["vehicle"], $data['iorder']));
            if (!$dbr)
                throw new CmsException('SQL ERROR: ' . $db->ErrorMsg() . ' -- ' . $db->sql);
            $this->_data['id'] = $db->Insert_ID();
            $this->_dirty = FALSE;
        }
    }

    private function _update() {
        global $mleblock;
        $data = $this->_data;
        $db = cmsms()->GetDb();
        $query = 'UPDATE ' . cms_db_prefix() . 'module_products_timelines
              SET title' . $mleblock . ' = ?, description' . $mleblock . ' = ?, meals = ?, vehicle = ?, iorder = ? WHERE id = ?';
        $dbr = $db->Execute($query, array($data['title' . $mleblock], $data['description' . $mleblock], $data["meals"], $data["vehicle"], $data['iorder'], $data['ids']));
        $this->_dirty = FALSE;
    }

    public function validate() {
        if (!isset($this->_data['product_id']) || $this->_data['product_id'] < 1) {
            throw new CmsException('Each products_timelines object needs a product_id');
        }
    }

    public function save() {
        global $mleblock;
        if (!$this->_dirty)
            return;
        $this->validate();
        if (isset($this->_data['ids']) && $this->_data['ids'] != '') {
            $this->_update();
        } else {
            $this->_insert();
        }
    }

    protected static function &load_from_data($data) {
        foreach (self::$_keys as $k) {
            if ($k != 'id' && $k != 'ids' && $k != 'iorder' && !in_array($k, array_keys($data))) {
                throw new CmsException($k . ' is missing from loaded timelines record');
            }
        }

        foreach (array_keys($data) as $key) {
            if (!in_array($key, self::$_keys)) {
                throw new CmsException('Unknown or extra data exists in the supploed timelines record');
            }
        }

        $ob = new products_timelines;
        $ob->_data = $data;
        $ob->_dirty = TRUE;
        return $ob;
    }

    public static function load_from_form($params) {
        global $mleblock;
        if (!isset($params['prodid'])) {
            throw new CmsException('Missing product id value from form');
        }

        if (!isset($params['title' . $mleblock]) && !is_array($params['title' . $mleblock])) {
            throw new CmsException('Missing title data from form');
        }

        if (!isset($params['description' . $mleblock]) && !is_array($params['description' . $mleblock])) {
            throw new CmsException('Missing description data from form');
        }

        // put everything together.
        $out = array();
        $keys = array('title_en', 'title_my', 'description_en', 'description_my', 'meals', 'vehicle', 'ids');
        for ($i = 0; $i < count($params['title' . $mleblock]); $i++) {
            $rec = array(
                'product_id' => (int) $params['prodid']
            );
            foreach ($keys as $key) {
                $rec[$key] = $params[$key][$i];
            }
            $obj = self::load_from_data($rec);
            $obj->_data['iorder'] = $i + 1;
            $out[] = $obj;
        }
        return $out;
    }

    public static function load_by_product($id) {
        global $mleblock;
        $db = cmsms()->GetDb();
        $query = 'SELECT * FROM ' . cms_db_prefix() . 'module_products_timelines
                  WHERE product_id = ? ORDER BY iorder';
        $dbr = $db->GetArray($query, array((int) $id));
        if (!is_array($dbr) || count($dbr) == 0)
            return;

        $res = array();
        foreach ($dbr as $one) {
            $res[] = self::load_from_data($one);
        }
        return $res;
    }

    public static function delete_by_product($id) {
        $db = cmsms()->GetDb();
        $query = 'DELETE FROM ' . cms_db_prefix() . 'module_products_timelines
              WHERE product_id = ?';
        $dbr = $db->Execute($query, array((int) $id));
        if (!$dbr)
            return FALSE;
        return TRUE;
    }

} // end of class

#
# EOF
#
?>