<?php
if( !isset($gCms) ) exit;
if (!$this->CheckPermission('Report Users') ) return;
$this->SetCurrentTab('report_users');

if (isset($_POST["filterapply"])) {
    if (isset($_POST['filterdate'])) set_site_preference('products_filterdate',trim(cleanValue($_POST["filterdate"])));
}

$userid = get_userid();
$where = "";
if ($this->CheckPermission('Report Users') && $userid != 1 && $userid != 7 && $userid != 3) {
    $where = "cms_users.user_id = $userid";
} else {
    $where = "cms_users.user_id not in(1,7)";
}

#Put together a list of current categories...
$entryarray = array();

$query = "SELECT DISTINCT
cms_module_products.`owner` AS `owner`,
cms_users.username,
cms_users.user_id
FROM
cms_module_products
INNER JOIN cms_users ON cms_users.user_id = cms_module_products.`owner`
WHERE $where";

$dbresult = $db->Execute($query);

$date = new DateTime();
$now = ($_POST['filterdate']) ? $_POST['filterdate'] : $date->format('Y-m-d');
set_site_preference('products_filterdate',$now);
//if($date1){
//    $now = ($date1) ? $date1 : $_POST['filterdate'];
//}else{
//    $date = new DateTime();
//    $now = $date->format('Y-m-d');
//}
$rowclass = 'row1';
$theme = cms_utils::get_theme_object();

while ($dbresult && $row = $dbresult->FetchRow()) {
    $onerow = new stdClass();
    $oneQuery = "SELECT count(*) as Counter FROM ".cms_db_prefix()."module_products WHERE details_vi != '' AND modified_date LIKE '$now%' and owner=".$row['owner'];
    $cnt = $db->GetRow($oneQuery);
    $onerow->owner = $row['username'];
    $onerow->cnt = $cnt['Counter'];
    $onerow->rowclass = $rowclass;
    
    $seoQuery = "SELECT count(*) as Counter FROM ".cms_db_prefix()."module_liseseo_item WHERE `alias` IS NOT NULL AND modified_time LIKE '$now%' AND owner=".$row['owner'];
    $seoCnt = $db->GetRow($seoQuery);
    $onerow->seoCnt = $seoCnt['Counter'];
    
    $onerow->edit_url = $this->CreateURL($id,'listedited',$returnid, array('compid'=>$row['user_id'],'date'=>$now,'cg_activetab'=>'report_users'));
    $onerow->edit_url_seo = $this->CreateURL($id,'listseo',$returnid, array('compid'=>$row['user_id'],'date'=>$now,'cg_activetab'=>'report_users'));
//    $mod = \cms_utils::get_module(MOD_PRODUCTS);

    $entryarray[] = $onerow;

    ($rowclass=="row1"?$rowclass="row2":$rowclass="row1");
  }
  
$date = new DateTime();
$now = $date->format('Y-m-d');
$this->smarty->assign('today', $now);
$this->smarty->assign('today_url', $this->CreateURL($id,'defaultadmin',$returnid, array('date'=>$now,'cg_activetab'=>'report_users')));
$this->smarty->assign('before', $now1);
$this->smarty->assign('before_url', $this->CreateURL($id,'defaultadmin',$returnid, array('date'=>$now1,'cg_activetab'=>'report_users')));
$this->smarty->assign_by_ref('items', $entryarray);
$this->smarty->assign('itemcount', count($entryarray));

$smarty->assign('filteruser',get_site_preference('adminlog_filteruser',''));
$smarty->assign('filteraction',get_site_preference('adminlog_filteraction',''));
$smarty->assign("langfilteruser",lang("filteruser"));
$smarty->assign("langfilteraction",lang("filteraction"));
$smarty->assign("langfilterapply",lang("filterapply"));
$smarty->assign("langfilterreset",lang("filterreset"));
$smarty->assign("langfilters",lang("filters"));
$smarty->assign("langshowfilters",lang("showfilters"));
$smarty->assign("filterdatevalue",get_site_preference("products_filterdate"));
$smarty->assign("actionUrl",$this->CreateURL($id,'defaultadmin',$returnid, array('cg_activetab'=>'report_users')));
$smarty->assign("filterdisplay",$filterdisplay);
$smarty->assign('SECURE_PARAM_NAME',CMS_SECURE_PARAM_NAME);
$smarty->assign('CMS_USER_KEY',$_SESSION[CMS_USER_KEY]);

#Display template
echo $this->ProcessTemplate('reportusers.tpl');


#
# EOF
#
?>
