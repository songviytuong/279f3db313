{if $itemcount gt 0}
  {$startform}
  <table cellspacing="0" class="pagetable">
    <thead>
      <tr>
        <th class='pageicon'>ID</th>
        <th>Type</th>
        <th>{$mod->Lang('sku')}</th>
        <th>{$mod->Lang('modifieddate')}</th>
        <th class='pageicon'></th>
      </tr>
    </thead>
    <tbody>
      {foreach from=$items key=lee item=entry}
	<tr id="item_{$entry->id}" class="{cycle values='row1,row2'}" style="cursor: move;">
        <td>{$lee+1}</td>
        <td><a href="{$entry->edit_url}">Products</a></td>
	<td><a href="{$entry->edit_url}">{$entry->sku}</a></td>
	<td>{$entry->modified_date}</td>
        <td><a href="{$entry->edit_url}">{admin_icon icon='edit.gif' alt=""}</a></td>
        </tr>
      {/foreach}
    </tbody>
  </table>
{$endform}
{/if}
<div class="pageoptions" style="height: 2em;">
    <div style="width: 40%; float: left; margin-top: 0.5em;">
        <a onclick="goBack()">{admin_icon icon='arrow-l.gif' alt=""} Go Back</a>
        <script>
        function goBack() {
            window.history.back();
        }
        </script>
    </div>
</div>
