<script type="text/javascript">
$(document).ready(function(){
  $('#toggle_filters').click(function(){
    $('#adminlog_filters').dialog({
      modal: true,
      width: 'auto'
    });
  });
})
</script>

    {admin_icon icon='view.gif' alt="{$langshowfilters}"} <a id="toggle_filters">{$langshowfilters} {$filterdatevalue}</a>

    <div id="adminlog_filters" style="display: none;" title="{$langfilters}">
        <form id="adminlog_filter" method="post" action="{$actionUrl}">
          <div class="pageoverflow">
            <p class="pagetext">{lang('showfilters')}:</p>
            <p class="pageinput"><input type="date" name="filterdate" value="{$filterdatevalue}"/></p>
          </div>
          <div class="pageoverflow">
            <p class="pagetext"></p>
            <p class="pageinput">
	      <input type="submit" name="filterapply" value="{lang('apply')}"/>
	      <input type="submit" name="filterreset" value="{lang('filterreset')}"/>
	    </p>
          </div>
        </form>
    </div>
| <a href="{$today_url}">{lang('todayfilters')}</a>
{if $itemcount > 0}
    <table cellspacing="0" class="pagetable">
        <thead>
            <tr>
                <th>{$mod->lang('username')}</th>
                <th class="pagew10" style="text-align: center !important;">{$mod->lang('post_count')}</th>
                <th class="pagew10 {literal}{sorter: false}{/literal}">&nbsp;</th>
                <th class="pagew10" style="text-align: center !important;">{$mod->lang('seo_count')}</th>
                <th class="pagew10 {literal}{sorter: false}{/literal}">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$items item=entry}
                <tr class="{$entry->rowclass}" onmouseover="this.className = '{$entry->rowclass}hover';" onmouseout="this.className = '{$entry->rowclass}';">
                    <td>{$entry->owner}</td>
                    <td style="text-align: center !important;"><a href="{$entry->edit_url}">{$entry->cnt}</a></td>
                    <td style="text-align: center !important;"><a href="{$entry->edit_url}">{admin_icon icon='view.gif' alt="{$langshowfilters}"} {$mod->lang('view')}</a></td>
                    <td style="text-align: center !important;"><a href="{$entry->edit_url_seo}">{$entry->seoCnt}</a></td>
                    <td style="text-align: center !important;"><a href="{$entry->edit_url_seo}">{admin_icon icon='view.gif' alt="{$langshowfilters}"} {$mod->lang('view')}</a></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}