{$langList}<br/><br/>
<div id="filterform">
{$formstart}
<fieldset style="width: 49%; float: left;">
  <legend>{$mod->Lang('filters')}:&nbsp;</legend>
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('productname')}:</p>
    <p class="pageinput">
       <input type="text" name="{$actionid}input_productname" value="{$filter.productname}" maxlength="255"/>
       <br/>{$mod->Lang('info_filter_productname')}
    </p>
  </div>
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('sku')}:</p>
    <p class="pageinput">
       <input type="text" name="{$actionid}input_sku" value="{$filter.sku}" maxlength="10"/>
       <br/>{$mod->Lang('info_filter_sku')}
    </p>
  </div>
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('hierarchy')}:</p>
    <p class="pageinput">{$input_hierarchy}&nbsp;{$mod->Lang('include_children')} {$input_children}</p>
  </div>
  {if isset($category_list)}
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('category')}:</p>
    <p class="pageinput">
      <select name="{$actionid}categories[]" multiple="multiple" size="5">
        {html_options options=$category_list selected=$filter.categories}
      </select>
    </p>
  </div>
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('exclude_categories')}:</p>
    <p class="pageinput">{$input_excludecats}</p>
  </div>
  {/if}
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('page_limit')}:</p>
    <p class="pageinput">{$input_pagelimit}</p>
  </div>
</fieldset>
<fieldset style="width: 47%; float: right;">
  <legend>{$mod->Lang('view')}:&nbsp;</legend>
  {if isset($fields_viewable)}
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('viewable_fields')}:</p>
    <p class="pageinput">
      <select name="{$actionid}custom_fields[]" size="3" multiple="multiple">
      {html_options options=$fields_viewable selected=$filter.custom_fields}
      </select>
    </p>
  </div>
  {/if}

  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('sort_by')}:</p>
    <p class="pageinput">{$input_sortby}</p>
  </div>
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('sort_order')}:</p>
    <p class="pageinput">{$input_sortorder}</p>
  </div>
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('translate_to_vn')}:</p>
    <p class="pageinput">{$input_translates}</p>
  </div>
  
  <div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('seo_include')}:</p>
    <p class="pageinput">{$input_seo_include}</p>
  </div>
  
  <div class="pageoverflow">
    <p class="pagetext">{$statustext}:</p>
    <p class="pageinput">{$input_status}</p>
  </div>
</fieldset>
<div class="pageoverflow">
  <p class="pageinput">
    <input type="submit" name="{$mod->GetActionId()}submit" value="{$mod->Lang('submit')}">
    <input type="submit" name="{$mod->GetActionId()}reset" value="{$mod->Lang('reset')}">
  </p>
</div>
<br/>
{$formend}
</div><!-- filterform -->


<div id="productlist">
<script type="text/javascript">{literal}
$(document).ready(function(){
  $('#filterform').hide();
  $('#filterbox').click(function(){
    $('#filterform').toggle();
  });
  $('#select_all_products').click(function(){
    var checked = $(this).attr('checked');
    if( checked == 'checked' ) {
      $('.multiselect').attr('checked',checked);
    } else {
      $('.multiselect').removeAttr('checked');
    }
  });
  
          
    

  $('.multiselect').click(function(){
    $('#select_all_products').removeAttr('checked');
  });
  $('#bulkaction_submit').click(function(){
    var len = $('#productlist tbody input:checkbox:checked').length;
    if( len == 0 )
    {
      alert('{/literal}{$mod->Lang('nothing_selected')}{literal}');
      return false;
    }
    return true;
  });
  
  $('.import_seo').click(function () {
        var sku = $(this).attr('id');
        $.ajax({
                url: '{/literal}{module_action_url action='admin_ajax_importseo' forajax=1}{literal}',
                method: 'POST',
                data: {
                  'sku': sku,
                },
                beforeSend: function() {
                    
                },
                success: function(res) {
                   if(res == "OK"){
                       location.reload();
                   }else{
                       console.log(res);
                   }
                }
            });
    });
    
    $('#search_sku').blur(function () {
        var sku = $(this).val();
        if(sku != ""){
            $.ajax({
                url: '{/literal}{module_action_url action='admin_ajax_exists' forajax=1}{literal}',
                method: 'POST',
                type: 'JSON',
                data: {
                  'sku': sku,
                  'aAction':'sku',
                  'aList' : 'productlist'
                },
                success: function(res) {
                    if (res) {
                        $('.loading').html(res['msg_loading']);
                        if (res['url']) {
                            setTimeout(function() {
                                window.location = res['url'];
                            }, 700)
                        }
                    }
                    return false;
                }
            });
        }
            
        });
        
});
{/literal}</script>

<div class="c_full">
  <div class="grid_6" style="padding-bottom: 0.25em;">
    <ul class="option-menu-horiz">
    
      <li>
        <a href="{module_action_url action=addproduct}">{cgimage image='icons/system/newobject.gif' alt=''} {$mod->Lang('addproduct')}</a>
      </li>
    {if $mode == "master"}  
      <li>
        <a href="{module_action_url action=importproducts}">{cgimage image='icons/system/import.gif' alt=''} {$mod->Lang('import_from_csv')}</a>
      </li>
      {if $itemcount gt 0}
        <li>
        <a href="{module_action_url action=exportcsv}">{cgimage image='icons/system/export.gif' alt=''} {$mod->Lang('export_to_csv')}</a>
        </li>
      {/if}
    {/if}
      <li><a id="filterbox">{cgimage image='icons/system/view.gif' alt=''} {$mod->Lang('filter')}</a></li>
    </ul>
  </div>

  <div class="grid_6">
    {if $itemcount gt 0}
      <span style="float: right; text-align: right;">
      {if isset($firstpage_url)}
        <a href="{$firstpage_url}" title="{$mod->Lang('firstpage')}">{$mod->Lang('firstpage')}</a>
        <a href="{$prevpage_url}" title="{$mod->Lang('prevpage')}">{$mod->Lang('prevpage')}</a>
      {/if}
      {if isset($firstpage_url) || isset($lastpage_url)}
        {$mod->Lang('page_of',$pagenumber,$pagecount)}
      {/if}
      {if isset($lastpage_url)}
        <a href="{$nextpage_url}" title="{$mod->Lang('nextpage')}">{$mod->Lang('nextpage')}</a>
        <a href="{$lastpage_url}" title="{$mod->Lang('lastpage')}">{$mod->Lang('lastpage')}</a>
      {/if}
      </span>
    {/if}
  </div>
  <div class="clearb"></div>
</div>

{if $itemcount gt 0}
  {$formstart2}
  <table cellspacing="0" class="pagetable">
    <thead>
      <tr>
	<th>{$idtext}</th>
	<th>{$producttext}</th>
        {if $allowedtravel == 1}
        <th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
        {/if}
        <th style="text-align:center;">SEO</th>
        <th></th>
        <th>SKU</th>
        <th><input type="text" name="{$actionid}sku" id="search_sku" placeholder="{$mod->Lang('search_by_sku')}" style="width:90%;"/> <small class="loading"></small></th>
	<th style="text-align:right !important;">{$pricetext}</th>
	<th style="text-align:right !important;">{$mod->Lang('status')}</th>
	<th style="text-align:right !important;">{$mod->Lang('last_modified')}</th>
        {if isset($filter.custom_fields)}
          {foreach from=$filter.custom_fields item='fid'}
            <th>{$fields_viewable.$fid}</th>
          {/foreach}
        {/if}
	<th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
	<th class="pageicon {literal}{sorter: false}{/literal}">{$mod->Lang('owner')}</th>
        {if $mode == "master"}
	<th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
        {/if}
	<th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
        {if $mode == "master"}
        <th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
	<th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
        <th class="pageicon {literal}{sorter: false}{/literal}"><input type="checkbox" title="{$mod->Lang('select_all')}" value="1" name="select_all" id="select_all_products"></th>
        {/if}
      </tr>
    </thead>
    <tbody>
      {foreach from=$items item=entry}
{*      <pre>{$entry|@print_r}</pre>*}
	<tr id="item_{$entry.id}" class="{cycle values='row1,row2'}">
	<td>{$entry.id}</td>
	<td><a href="{$entry.edit_url}" title="{$mod->Lang('edit')}">{if !empty($entry.product_name_vi)}<font color="red">{elseif $entry.status|capitalize == 'Draft'}<font color="green">{/if}{$entry.product_name}{if !empty($entry.product_name_vi)}</font>{elseif $entry.status|capitalize == 'Draft'}</font>{/if}</a></td>
        {if $allowedtravel == 1}
        <td>{module_action_link module='Products' action='admin_edit_timelines' prodid=$entry.id image='calendar.png' text=$mod->Lang('edit_options') imageonly=1}</td>
        {/if}
        <td style="text-align:center;">{if $entry.edit_seo}<a href="{$entry.edit_seo}" {if ($entry.edit_seo_target)}target="_blank"{/if}>{/if}{admin_icon icon={$entry.icon} alt="SEO" class="{if ($entry.icon_class)}import_seo{/if}" id="{$entry.sku}" title="Import SEO"}{if $entry.edit_seo}</a>{/if}</td>
        <td>{if $entry.source != ""}<a href="https://theodorealexander.com/ProductDetail.aspx?{$entry.source}" target="_blank"><img src="themes/OneEleven/images/icons/system/view.gif" class="systemicon">{/if}</td>
        <td>{$entry.sku}</td>
        
        <td>{if $mode == "master"}
            <a href="../download.php?sku={$entry.sku}_scale_1&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_scale_1" title="Scale">{admin_icon icon='scale.png' alt=''}</a> - 
            <a href="../download.php?sku={$entry.sku}_main_1&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_main_1%3F%24%26rect=0,0,1500,1500%26scl=2" title="Main">{admin_icon icon='main.png' alt=''}</a> - 
            <a href="../download.php?sku={$entry.sku}_more_1&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_more_1%3F%24%26rect=0,0,870,870%26scl=1" title="More 1">{admin_icon icon='small.png' alt=''}</a> 
            <a href="../download.php?sku={$entry.sku}_more_2&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_more_2%3F%24%26rect=0,0,870,870%26scl=1" title="More 2">{admin_icon icon='small.png' alt=''}</a> 
            <a href="../download.php?sku={$entry.sku}_more_3&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_more_3%3F%24%26rect=0,0,870,870%26scl=1" title="More 3">{admin_icon icon='small.png' alt=''}</a> 
            <a href="../download.php?sku={$entry.sku}_more_4&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_more_4%3F%24%26rect=0,0,870,870%26scl=1" title="More 4">{admin_icon icon='small.png' alt=''}</a> - 
            <a href="../download.php?sku={$entry.sku}_main_1_14&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_main_1%3F%24%26rect=0,0,2000,2000%26scl=1" title="1/4">{admin_icon icon='main14.jpg' alt=''}</a> 
            <a href="../download.php?sku={$entry.sku}_main_1_24&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_main_1%3F%24%26rect=2000,0,1000,2000%26scl=1" title="2/4">{admin_icon icon='main24.jpg' alt=''}</a> 
            <a href="../download.php?sku={$entry.sku}_main_1_34&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_main_1%3F%24%26rect=0,2000,2000,1000%26scl=1" title="3/4">{admin_icon icon='main34.jpg' alt=''}</a> 
            <a href="../download.php?sku={$entry.sku}_main_1_44&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_main_1%3F%24%26rect=2000,2000,1000,1000%26scl=1" title="4/4">{admin_icon icon='main44.jpg' alt=''}</a> - 
            <a href="../download.php?sku=1_{$entry.sku}_main_1&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_main_1%3F%24ProductListList%24" title="1">{admin_icon icon='1.png' alt=''}</a> 
            <a href="../download.php?sku=2_{$entry.sku}_main_1&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_main_1%3F%24ProductListGrid%24" title="2">{admin_icon icon='2.png' alt=''}</a> 
            <a href="../download.php?sku=3_{$entry.sku}_main_1&file=http://s7d5.scene7.com/is/image/TheodoreAlexander/{$entry.sku}_main_1%3F%24ProductPage%24" title="3">{admin_icon icon='3.png' alt=''}</a>
	{/if}</td>
	<td style="text-align:right !important;">{$entry.price}</td>
        <td style="text-align:right !important;">{if $entry.status|capitalize == 'Draft'}
              <span style="color: red;">{$mod->Lang($entry.status)}</span>
            {else}
               {$mod->Lang($entry.status)}
            {/if}
        </td>
	<td style="text-align:right !important;">{$entry.modified_date|cms_date_format:'%d/%m/%Y %T'}</td>
        {if isset($filter.custom_fields) && count($filter.custom_fields)}
          {foreach from=$filter.custom_fields item='fid'}
            <td>{capture assign='tmp'}{$field_names.$fid}{/capture}
              {$entry.$tmp}</td>
          {/foreach}
        {/if}
        <td>{$entry.position}</td>
        <td>{$entry.owner}</td>
        {if $mode == "master"}
	<td>{module_action_link module='Products' action='admin_edit_attribs' prodid=$entry.id image='table_relationship.png' text=$mod->Lang('edit_options') imageonly=1}</td>
        {/if}
	<td>{$entry.editlink}</td>
        {if $mode == "master"}
	<td>{$entry.copylink}</td>
	<td>{$entry.deletelink}</td>
        <td><input type="checkbox" class="multiselect" name="{$actionid}multiselect[]" value="{$entry.id}"></td>
        {/if}
        </tr>
      {/foreach}
    </tbody>
  </table>

  <div class="pageoptions" style="height: 2em;">
    <div style="width: 40%; float: left; margin-top: 0.5em;">
      {$addlink}{if $mode == "master"}&nbsp;{$importlink}{/if}
    </div>
    {if $itemcount gt 0 && $mode == "master"}
      <div style="text-align: right; width: 40%; float: right; margin-top: 0.5em; margin-bottom: 0.5em;">
        {$mod->Lang('with_selected')}:
        <select name="{$actionid}bulkaction">{html_options options=$bulkactions}</select>
        <input type="submit" id="bulkaction_submit" name="{$actionid}submit" value="{$mod->Lang('go')}"/>
      </div>
    {/if}
  </div>
{$formend2}
{/if}
</div><!-- productlist -->
