{$startform}
<fieldset>
    
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('allowed_travel_module')}:</p>
  <p class="pageinput">{cge_yesno_options prefix=$actionid name='allowedtravel' selected=$allowedtravel}</p>
</div>

<legend>{$mod->Lang('general_settings')}:</legend>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_urlprefix')}:</p>
  <p class="pageinput"><input type="text" name="{$actionid}urlprefix" size="20" maxlength="20" value="{$urlprefix}"/></p>
</div>
{if isset($input_currencysymbol)}
<div class="pageoverflow">
  <p class="pagetext">{$prompt_currencysymbol}:</p>
  <p class="pageinput">{$input_currencysymbol}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_weightunits}:</p>
  <p class="pageinput">{$input_weightunits}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_lengthunits}:</p>
  <p class="pageinput">{$input_lengthunits}</p>
</div>
{/if}
<div class="pageoverflow">
  <p class="pagetext"></p>
  <p class="pageinput">
    <input type="submit" name="{$actionid}setecomhandlers" value="{$mod->Lang('btn_setecomhandlers')}"/>
    <br/>
    {$mod->Lang('info_setecomhandlers')}
  </p>
</div>
</fieldset>

<fieldset>
<legend>{$mod->Lang('product_editing_settings')}:</legend>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_sku_required')}:</p>
  <p class="pageinput">
    {cge_yesno_options prefix=$actionid name='skurequired' selected=$skurequired}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_default_taxable')}:</p>
  <p class="pageinput">{$input_taxable}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_default_status')}:</p>
  <p class="pageinput">{$input_status}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_allowed_filetypes')}:</p>
  <p class="pageinput">{$input_allowed_filetypes}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_deleteproductfiles')}:</p>
  <p class="pageinput">{$input_deleteproductfiles}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_slugtemplate')}:</p>
  <div class="pageinput">
    <textarea name="{$actionid}slugtemplate" rows="3" cols="80">{$slugtemplate}</textarea>
    {$mod->Lang('info_slugtemplate')}
  </div>
</div>
</fieldset>

<fieldset>
<legend>{$mod->Lang('product_summary_settings')}:</legend>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_summary_newdefault')}:</p>
  <p class="pageinput">
    {cge_yesno_options prefix=$actionid name='summary_newdefault' selected=$summary_newdefault}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_summarypagelimit')}:</p>
  <p class="pageinput">
    <input type="text" name="{$actionid}summary_pagelimit" size="5" maxlength="5" value="{$summary_pagelimit}"/>
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_summarysortorder}:</p>
  <p class="pageinput">{$input_summarysortorder}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_summarysorting}:</p>
  <p class="pageinput">{$input_summarysorting}</p>
</div>
</fieldset>

<fieldset>
<legend>{$mod->Lang('product_detail_settings')}:</legend>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_usehierpathurls')}:</p>
  <p class="pageinput">
    {$input_usehierpathurls}
    <br/>{$mod->Lang('deprecated')}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_detailpage}:</p>
  <p class="pageinput">{$input_detailpage}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_use_detailpage_for_search')}:</p>
  <p class="pageinput">
    {$input_use_detailpage_for_search}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_notfound_behavior')}:</p>
  <p class="pageinput">
    <select name="{$actionid}prodnotfound">
    {html_options options=$notfound_opts selected=$prodnotfound}
    </select>
    <br/>
    {$mod->Lang('info_prodnotfound')}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_notfound_page')}:</p>
  <p class="pageinput">
    {$input_prodnotfoundpage}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_notfound_msg')}:</p>
  <p class="pageinput">
    <textarea name="{$actionid}prodnotfoundmsg" rows="5">{$prodnotfoundmsg}</textarea>
  </p>
</div>
</fieldset>

<fieldset>
<legend>{$mod->Lang('hierarchy_settings')}:</legend>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_hierpage')}:</p>
  <p class="pageinput">{$input_hierpage}<br/>
  {$mod->Lang('info_hierpage')}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_prettyhierurls')}:</p>
  <p class="pageinput">{cge_yesno_options prefix=$actionid name='prettyhierurls' selected=$prettyhierurls}<br/>
  {$mod->Lang('info_prettyhierurls')}
  </p>
</div>
</fieldset>

<fieldset>
<legend>{$mod->Lang('image_handling_settings')}:</legend>
<p style="color: red;">{$mod->Lang('info_imagestuff_deprecated')}</p>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('prompt_allowed_imagetypes')}:</p>
  <p class="pageinput">{$input_allowed_imagetypes}</p>
</div>
</fieldset>

<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">{$submit}</p>
</div>

{$endform}