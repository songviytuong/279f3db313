jQuery(function($){
	initAjax();
	initAjaxEvents();
});
function initAjax(){

	jQuery('.init-ajax-toggle').children("a").addClass('ajax-toggle');
	jQuery('.init-ajax-delete').children("a").addClass('ajax-delete');
}

function initAjaxEvents() {

	jQuery('.ajax-toggle').click(function (event) {
            event.preventDefault();		
            ajax_toggle($(this));
	});
	
	jQuery('.ajax-delete').click(function (event) {
            event.preventDefault();
            if(confirm('Are you sure?')) {
                ajax_delete($(this));
            }
	});		
}

/**
* TOGGLE ACTIVE
* @ obj : jQuery DOM object
*/
function ajax_toggle(obj) {

	var url = obj.attr("href");
	url += "&showtemplate=false";
	jQuery.ajax({
		type: "GET",
		url: url,
		async: true,
		dataType: "json",
		beforeSend: function() {
		
			obj.empty().append('<div class="ajax-loading ajax-loader-type-16"></div>');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			
			//alert("Sorry. There was an LISE AJAX error: " + textStatus);
		},			
		success: function(data) {
			obj.html(data.image);				
			obj.attr("href", data.href);
			obj.attr("target", data.href_target);
                        obj.removeClass('init-ajax-toggle');
		},
		complete: function() {
                    setTimeout(function(){
                        $(this, ".ajax-loading").remove();
                    },2000);
		}		
		
	});
}