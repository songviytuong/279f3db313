{if isset($item_list)}

<table id="item_list" class="pagetable sort_table">

	<thead>
	
		<tr>

			<th>
				{$mod->Lang('name')}
			</th>
			
			<th class="pageicon">
				{$mod->Lang('status')}
			</th>
			
			<th class="pageicon">
			</th>
            
            <th class="pageicon">
			</th>
			
			<th class="pageicon">
			</th>

		</tr>
	
	</thead>
	
	<tbody>
	
	{foreach $item_list as $item}

		{cycle values="row1,row2" assign=rowclass}
		<tr id="item_{$item.id}" class="{$rowclass}">
		
			<td>
                <a href="{cms_action_url action="admin_item_edit" item_id=$item.id}" title="{$mod->Lang('edit')}">
                    {$item.name}
                </a>
			</td>
			
			<td class="pagepos">
                <a href="{cms_action_url action="admin_item_toggle" item_id=$item.id}" title="{$mod->Lang('status_change')}">
                    {if $item.status == true}
                        {admin_icon icon='true.gif'}
                    {else}
                        {admin_icon icon='false.gif'}
                    {/if}
                </a>
			</td>
			
			<td class="pagepos">
				<a href="{cms_action_url action="admin_item_edit" item_id=$item.id}" title="{$mod->Lang('edit')}">
                    {admin_icon icon='edit.gif'}
                </a>
			</td>
            
            <td class="pagepos">
				<a href="{cms_action_url action="admin_item_export" item_id=$item.id}" title="{$mod->Lang('export')}">
                    {admin_icon icon='export.gif'}
                </a>
			</td>
			
			<td class="pagepos">
				<a class="item_delete" href="{cms_action_url action="admin_item_delete" item_id=$item.id}" title="{$mod->Lang('delete')}">
                    {admin_icon icon='delete.gif'}
                </a>
			</td>
			
		</tr>

	{/foreach}

	</tbody>

</table>

{if isset($item_list) AND $item_list|@count > 1}
{form_start action="admin_item_sort" item_sort=""}
    <input type="submit" name="{$actionid}submit_sort" value="{$mod->Lang('sort_save')}">
{form_end}
{/if}

{else}

<p>
	{$mod->Lang('item_list_empty')}
</p>

{/if}

<div class="pageoverflow">
	<p>
		<a href="{cms_action_url action="admin_item_edit"}" title="{$mod->Lang('add')}">
            {admin_icon icon='newobject.gif'} {$mod->Lang('item_add')}
        </a>
	</p>
</div>

{if isset($item_list) AND $item_list|@count > 0}
{literal}
<script type="text/javascript">
    $(document).ready(function(){
    
        $('a.item_delete').click(function(){
            return confirm('{/literal}{$mod->Lang('item_delete_confirm')}{literal}');
        })
        
        $("#item_list.sort_table").tableDnD({
            onDragClass: "row1hover",
            onDrop: function(table, row) {
                $("#item_list").find("tbody tr").removeClass();
                $("#item_list").find("tbody tr:nth-child(2n+1)").addClass("row1");
                $("#item_list").find("tbody tr:nth-child(2n)").addClass("row2");
                var rows = table.tBodies[0].rows;
            var sortseq = '' ;
            for (var i=0; i<rows.length; i++) {
                sortseq += rows[i].id+"|";
            }
                $('input[name={/literal}{$actionid}{literal}item_sort]').val(sortseq) ;
            }
        });
        
    });
</script>
{/literal}
{/if}