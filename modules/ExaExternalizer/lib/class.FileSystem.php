<?php

namespace ExaExternalizer;

class FileSystem
{
   
   
   
   
   
   
   
   

/* ************************************************************************** *\
   Cr�ation d'un dossier
\* ************************************************************************** */

public static function createFolder($folder='')
{
   
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $mod = \cms_utils::get_module('ExaExternalizer');
    $chmod = $mod->GetPreference('chmod');
    $folder_path = cms_join_path($cache_path, $folder);

    // V�rification de l'existance du dossier
    if (!file_exists($folder_path)):

        if (mkdir($folder_path, octdec($chmod), true)):
            chmod($folder_path, octdec($chmod));
        endif;

    endif;
   
    // Si le dossier existe, v�rifier qu'il dispose des droits d'�criture
    if (file_exists($folder_path)):

        if (!is_writable($folder_path)):
            return false;
        endif;

    endif;

    // R�initialiser le timeout
    \ExaExternalizer\TimeOut::reset() ;

    // Cr�ation d'un fichier index.html vide
    self::createIndex($folder) ;

    return true;
   
}









/* ************************************************************************** *\
   Suppression d'un dossier
\* ************************************************************************** */

public static function deleteFolder($folder='')
{
   
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $folder_path = cms_join_path($cache_path, $folder);


    // Suppression
    @recursive_delete($folder_path);

    return true;
   
}









/* ************************************************************************** *\
   Cr�ation d'un fichier index.html
\* ************************************************************************** */

public static function createIndex($folder='')
{
   
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $folder_path = cms_join_path($cache_path, $folder);

    $dummy = cms_join_path($folder_path,'index.html');

    if (!file_exists($dummy)):

        if (!touch($dummy)):
            return false;
        endif;

    endif;

    return true;
   
}









/* ************************************************************************** *\
   �chappe un nom de fichier
\* ************************************************************************** */

public static function escapeFilename($filename='')
{
   
    if (function_exists("transliterator_transliterate")):
        return \transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $filename);
    else:
        return \munge_string_to_url($filename);
    endif;
}









/* ************************************************************************** *\
   Cr�ation d'un fichier
\* ************************************************************************** */

public static function createFile($folder='', $name='dummy', $ext='txt', $content='', $date='')
{
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
   
    // V�rification des param�tres
    if (empty($folder)):
        $return->message = 'No folder passed!';
        return $return;
    endif;

    if (empty($name)):
        $return->message = 'No name passed!';
        return $return;
    endif;

    if (empty($ext)):
        $return->message = 'No extension passed!';
        return $return;
    endif;
    
    // V�rification du dossier d'�criture
    \ExaExternalizer\FileSystem::createFolder($folder);
    
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $folder_path = cms_join_path($cache_path, $folder);

    // Pr�paration du nom du fichier
    $name = self::escapeFilename($name);

    // Cr�ation du fichier
    $fname = cms_join_path($folder_path, $name.'.'.$ext);
    file_put_contents($fname, $content);

    // Rendre chaque fichier disponible en �criture
    chmod($fname, 0666);

    // Modifier la date du fichier avec la date de modification du template
    touch($fname, $date);

    $return->result = true;
    return $return;
   
}









/* ************************************************************************** *\
   Lecture d'un fichier
\* ************************************************************************** */

public static function readFile($folder='', $name='dummy', $ext='txt', $date_check=true)
{
   
    $return = false;
   
    // V�rification des variables
    if (empty($folder)):
        return false;
    endif;

    if (empty($name)):
        return false;
    endif;

    if (empty($ext)):
        return false;
    endif;
   
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $folder_path = cms_join_path($cache_path, $folder);

    // Lecture du fichier
    $fname = cms_join_path($folder_path, $name.'.'.$ext);
    $return = file_get_contents($fname);

    return $return;
   
}









}
?>