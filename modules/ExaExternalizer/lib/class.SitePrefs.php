<?php

namespace ExaExternalizer;

class SitePrefs
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Variables
\* ************************************************************************** */

public static $fields = [
    'sitepref_name',
    'sitepref_value'
    ];









/* ************************************************************************** *\
    Get
\* ************************************************************************** */

public static function get($options=[]) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->count = null;
    $return->record = [];
    
    // Default options
    $default = [
        'fields' => '*',
        'where' => null,
        'order' => null,
        'limit' => null,
        'offset' => null
        ];
      
    // Filter options
    foreach ($options as $key => $val):
        if (!array_key_exists($key, $default)):
            unset($options[$key]);
        endif;
    endforeach;
    $options = array_merge($default, $options);
    
    // Query
    $sql = "SELECT {$options['fields']} FROM " . CMS_DB_PREFIX . "siteprefs";
    $sql .= (!empty($options['where'])) ? " WHERE {$options['where']}" : '';
    $sql .= (!empty($options['order'])) ? " ORDER BY {$options['order']}" : '';
    $sql .= (!empty($options['limit'])) ? " LIMIT {$options['limit']}" : '';
    $sql .= (!empty($options['offset'])) ? " OFFSET {$options['offset']}" : '';
    
    $result = $db->Execute($sql);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->count = $result->RecordCount();
    $return->record = $result->GetAll();
    $return->result = true;
    
    return $return;
    
}









/* ************************************************************************** *\
    Set
\* ************************************************************************** */

public static function set($options) {
    
    // Pr�paration des options
    foreach ($options as $key => $val):
        
        // Filtrage
        if (!in_array($key, self::$fields)):
            unset($options[$key]);
            continue;
        endif;

        // Optimisation
        if ($val == ''):
            $options[$key] = NULL;
        endif;
        
        switch ($key):
            case 'sitepref_name':
            case 'sitepref_value':
                $options[$key] = trim($val);
                break;
        endswitch;
        
    endforeach;
    
    // Mise � jour
    if (empty($options['sitepref_name'])):
        return false;
    endif;
    
    $return = self::update($options);
    
    return $return;

}









/* ************************************************************************** *\
    Update
\* ************************************************************************** */

protected static function update($options) {
    
    $mod = \cms_utils::get_module('ExaExternalizer');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    $timeStamp = time();
    $timeNow = date("Y-m-d H:i:s",$timeStamp);
    
    // Pr�paration des valeurs
    $options['modified_date'] = (empty($options['modified_date'])) ? $timeNow : $options['modified_date'];

    // Pr�paration du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requ�te
    $query = "
        UPDATE " . CMS_DB_PREFIX . "siteprefs
        SET {$set}
        WHERE sitepref_name = '{$options['sitepref_name']}'
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = $mod->Lang('sitepref_edited');
    
    return $return;
    
}









/* ************************************************************************** *\
    Exportation
\* ************************************************************************** */

public static function export() {
    
    // Variables
    $folder = '_SitePrefs';
    $mod = \cms_utils::get_module('ExaExternalizer');
    $template_extension = $mod->GetPreference('template_extension');

    // Cr�ation du dossier d'exportation
    \ExaExternalizer\FileSystem::createFolder($folder);
    
    $result = self::get();
    if ($result->count > 0):

        $item_record = $result->record;

        foreach ($item_record as &$item):

            switch ($item['sitepref_name']):
                case 'sitedownmessage':
                case 'metadata':
                case 'sitename':
                case 'Search_mapi_pref_stopwords':
                    \ExaExternalizer\FileSystem::createFile($folder, $item['sitepref_name'], $template_extension, $item['sitepref_value'], $item['modified_date']);
                    break;
            endswitch;

        endforeach;

    endif;
    
}









/* ************************************************************************** *\
   Importer les gabarits
\* ************************************************************************** */
public static function import()
{
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $mod = \cms_utils::get_module('ExaExternalizer');
    $template_extension = $mod->GetPreference('template_extension');
    $folder = '_SitePrefs';
    $folder_path = cms_join_path($cache_path, $folder);


    // Parcourir chaque gabarit
    $result = self::get();
    if (count($result->record) > 0):

        $item_record = $result->record;

        foreach ($item_record as $item):

            switch ($item['sitepref_name']):
                case 'sitedownmessage':
                case 'metadata':
                case 'sitename':
                case 'Search_mapi_pref_stopwords':
                
                    $filename = cms_join_path($folder_path, $item['sitepref_name'] . '.' . $template_extension);

                    // Si le fichier n'existe pas
                    if (!file_exists($filename)):
                        echo "caca";
                        continue;
                    endif;

                    // Si le fichier n'a pas �t� modifi�
                    $filetime = @filemtime($filename);
                    if ($filetime <= $item['modified_date']):
                        continue;
                    endif;

                    // Lecture du contenu
                    $content = '';
                    $filesize = filesize($filename);
                    $fp = fopen($filename, 'r');
                    $content = fread($fp, $filesize);
                    fclose($fp);

                    // Sauvegarde du contenu
                    $result = self::set([
                        'sitepref_name' => $item['sitepref_name'],
                        'sitepref_value' => $content,
                        'modified_date' => $filetime
                        ]);

                    // Mise � z�ro du timeout
                    \ExaExternalizer\TimeOut::reset();
                    
                    // Effacement du cache CMSMS
                    cmsms()->clear_cached_files(-1);
                    
                    break;
                    
                endswitch;

        endforeach;

    endif;
    
    $return->result = true;
    
    return $return;
   
}









}?>