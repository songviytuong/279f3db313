<ul>
    <li>
        <strong>Version 0.6</strong>
        <ul>
            <li>
                Bug #10824 corrigé (importation des gabarits de modules)
            </li>
        </ul>
    </li>
    <li>
        Version 0.5.2
        <ul>
            <li>
                Correction d'un bug lors de la mise à jour du module si celui-ci
                était activé lors de la procédure
            </li>
        </ul>
    </li>
    <li>
        Version 0.5.1
        <ul>
            <li>
                Bug #10856 corrigé
            </li>
        </ul>
    </li>
    <li>
        Version 0.5
        <ul>
            <li>
                Ajout d'une hiérarchie des gabarits
            </li>
        </ul>
    </li>
    <li>
        Version 0.4.1
        <ul>
            <li>
                Bug #10821 corrigé
            </li>
        </ul>
    </li>
    <li>
        Version 0.4
        <ul>
            <li>
                Bug #10786 corrigé
            </li>
            <li>
                Bug #10817 corrigé
            </li>
            <li>
                Optimisations mineurs du code
            </li>
        </ul>
    </li>
    <li>
        Version 0.3
        <ul>
            <li>
                Ajout d'évènements d'importation et d'exportation pour rendre
                des modules externes compatibles
            </li>
        </ul>
    </li>
    <li>
        Version 0.2
        <ul>
            <li>
                Refonte du code suivant les dernières guidelines officielles
            </li>
            <li>
                Ajout du support de l'export/import des contenus
            </li>
            <li>
                Ajout de la fonctionnalité de rafraichissement des exportations
            </li>
            <li>
                Ajout d'un onglet pour faire un don
            </li>
        </ul>
    </li>
    <li>
        Version 0.1
        <ul>
            <li>
                Première version compatible CMSMS 2.x
            </li>
            <li>
                Toutes les fonctionnalités de TemplateExternalizer ne sont pas
                encore intégrées
            </li>
            <li>
                Version Alpha ne devant pas être utilisée sur un site en
                production
            </li>
            <li>
                Diffusion restreinte aux utilisateurs du forum www.cmsmadesimple.fr
            </li>
        </ul>
    </li>
</ul>