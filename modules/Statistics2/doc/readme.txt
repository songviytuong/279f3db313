Statistics 1.1.3 -> Statistics2

IMPORTANT!

If you are using Statistics 1.1.3 do NOT uninstall this module before installing Statistics2! If Statistics is still installed, Statistics2 will copy the database-tables for its own use. After installing Statistics2 you can deinstall and remove Statistics.

