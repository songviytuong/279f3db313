<?php

class Sniffer {	
	var $useragent="";
	var $robot="";
	var $oses=array(
	  "win"=>"Windows",
		"linux"=>"Linux",
	  "mac"=>"MacOS",
	  "freebsd"=>"FreeBSD",
	  "netbsd"=>"NetBSD",
		"sun"=>"SunOS"
	);
	var $browsers=array(	  
		"firefox"=>"Firefox",
		"safari"=>"Safari",
	  "opera"=>"Opera",
		"chrome"=>"Chrome",
		"netscape"=>"Netscape",
		"konqueror"=>"Konqueror"				  
	);
	var $robots=array(
    "msnbot",
	  "cuill",
	  "crawl",
	  "search",
	  "exabot",
	  "msn",
    "googlebot",
    "yahoo",
    "jeeves"
	);
		
	function Sniffer() {
		$this->useragent=$_SERVER['HTTP_USER_AGENT'];
	}
	function GetBrowser($extrarobots="") {
		if (preg_match("/msie/i",$this->useragent)) {			
			if (preg_match("/6.0/i",$this->useragent)) return "Internet Explorer 6";
			if (preg_match("/7.0/i",$this->useragent)) return "Internet Explorer 7";
			if (preg_match("/8.0/i",$this->useragent)) return "Internet Explorer 8";
			return "Internet Explorer";
		}
		foreach ($this->browsers as $id=>$browser) {
  		if (preg_match('/'.$id.'/i',$this->useragent)) return $browser;
  	}
  	if ($this->IsRobot($extrarobots)) return "Robot (".$this->robot.")";
  	return "Other";
	}
	
  function GetOS() {
  	foreach ($this->oses as $id=>$os) {
  		if (preg_match('/'.$id.'/i',$this->useragent)) return $os;
  	}
    if ($this->IsRobot($extrarobots)) return "Robot (".$this->robot.")";
  	return "Other";
	}
	
	function IsRobot($extrarobots="") {
    $robots=$this->robots;
    if ($extrarobots!="") {
      $robots=array_merge($robots,explode(";",$extrarobots));
    }
		foreach ($robots as $id) {
			if (preg_match('/'.$id.'/i',$this->useragent)) {
				$this->robot=$id;
				return true;
			}
		}
		return false;
	}
}

?>