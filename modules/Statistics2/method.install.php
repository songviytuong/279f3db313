<?php
if (!isset($gCms)) exit;
$db = $this->GetDb();

// copy old Statistics-tables, if Statistics 1.1.3 is installed
function tblexists ($db,$tblname) {
  $query = "SELECT * FROM $tblname";
  if (!$db->Execute($query)) {
    return FALSE; 
  } else {
    return TRUE;
  }
}

if ((ModuleOperations::get_instance()->get_module_instance('Statistics',"2.0")!=null) and 
(tblexists($db, cms_db_prefix().'module_stat_data')) and 
(tblexists($db, cms_db_prefix().'module_stat_data_seq')) and 
(tblexists($db, cms_db_prefix().'module_stat_logstrings')) and 
(tblexists($db, cms_db_prefix().'module_stat_logstrings_seq')) and
(tblexists($db, cms_db_prefix().'module_stat_visitors')) and 
(tblexists($db, cms_db_prefix().'module_stat_visitors_seq')) and
(tblexists($db, cms_db_prefix().'module_stat_visitortrack')))
{
  $old = cms_db_prefix().'module_stat';
  $new = cms_db_prefix().'module_stat2';
  
  $query = 'CREATE TABLE '.$new.'_data SELECT * FROM '.$old.'_data';
  $result=$db->Execute($query);

  $query = 'CREATE TABLE '.$new.'_data_seq SELECT * FROM '.$old.'_data_seq';
  $result=$db->Execute($query);

  $query = 'CREATE TABLE '.$new.'_logstrings SELECT * FROM '.$old.'_logstrings';
  $result=$db->Execute($query);

  $query = 'CREATE TABLE '.$new.'_logstrings_seq SELECT * FROM '.$old.'_logstrings_seq';
  $result=$db->Execute($query);

  $query = 'CREATE TABLE '.$new.'_visitors SELECT * FROM '.$old.'_visitors';
  $result=$db->Execute($query);

  $query = 'CREATE TABLE '.$new.'_visitors_seq SELECT * FROM '.$old.'_visitors_seq';
  $result=$db->Execute($query);

  $query = 'CREATE TABLE '.$new.'_visitortrack SELECT * FROM '.$old.'_visitortrack';
  $result=$db->Execute($query);

}
else {
$dict = NewDataDictionary($db);
$taboptarray = array('mysql' => 'TYPE=MyISAM');

//Unified datalist
$flds = "
      id I KEY,
      name C(80),
      seckey C(255),
      value I,
      updated I
    ";
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_stat2_data", $flds, $taboptarray );
$dict->ExecuteSQLArray( $sqlarray );
$db->CreateSequence( cms_db_prefix()."module_stat2_data_seq" );

//Visitor tracking
$flds = "
      id I KEY,
      sessionid C(80),      
      remoteaddr C(32),
      remotehost C(255),
      lastpage I,
      lastseen I,
      pagecount I,
      referer C(255)
    ";
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_stat2_visitors", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_stat2_visitors_seq");

//Visitor track
$flds = "
      visitorid I,
      accesstime I,
      pageid I,
      info X
      ";
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_stat2_visitortrack", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);


//Log strings, for debugging
$flds = "
      id I key,
      time I,
      info C(255)
    ";
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_stat2_logstrings", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_stat2_logstrings_seq");

//Caching dns-lookups
/*$flds = "
      id I key,
      time I,
      ip C(32),
      host C(255)
    ";
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_stat2_ipcache", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_stat2_ipcache_seq");
*/
}

$this->CreatePermission('Administrate Statistics2', $this->Lang('permission'));
$this->CreatePermission('View Statistics2', $this->Lang('viewpermission'));

$this->SetPreference("emailtemplate",$this->GetEmailTemplate(true));
$this->SetPreference("topxtemplate",$this->GetTopXTemplate(true));
$this->SetPreference("activelimit",300);
$this->SetPreference("visitorlimit",1800);
$this->SetPreference("showdaylimit",31);
$this->SetPreference("showweeklimit",52);
$this->SetPreference("showmonthlimit",12);
$this->SetPreference("showyearlimit",10);
$this->SetPreference("showbrowserlimit",-1);
$this->SetPreference("showoslimit",-1);
$this->SetPreference("showcountrylimit",-1);
$this->SetPreference("charttype","text");
$this->SetPreference("ignoreips","");
$this->SetPreference("debuglog",0);
$this->SetPreference("last_reset",time());
$this->Audit(0,$this->lang('friendlyname'),$this->Lang('installmessage',$this->GetVersion()));

?>