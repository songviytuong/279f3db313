<?php
if (!isset($gCms)) exit;
$db = &$this->GetDb();
$dict = NewDataDictionary($db);
$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_stat2_data");
$dict->ExecuteSQLArray($sqlarray);
$db->DropSequence(cms_db_prefix()."module_stat2_data_seq");

$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_stat2_visitors");
$dict->ExecuteSQLArray($sqlarray);
$db->DropSequence(cms_db_prefix()."module_stat2_visitors_seq");

$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_stat2_visitortrack");
$dict->ExecuteSQLArray($sqlarray);

$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_stat2_logstrings");
$db->DropSequence(cms_db_prefix()."module_stat2_logstrings_seq");
$dict->ExecuteSQLArray($sqlarray);

$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_stat2_ipcache");
$db->DropSequence(cms_db_prefix()."module_stat2_ipcache_seq");
$dict->ExecuteSQLArray($sqlarray);

$this->RemovePermission('Administrate Statistics2');
$this->RemovePermission('View Statistics2');

$this->RemovePreference();

$this->Audit(0, $this->lang('friendlyname'), $this->lang('uninstallmessage',$this->GetVersion()));

?>