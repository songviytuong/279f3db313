<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20011 by Robert Campbell 
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
# 
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGURL_Service_Yourls extends CGURL_Service
{
  private $_url;
  private $_signature;

  public function get_name()
  {
    return 'yourls';
  }


  public function has_settings()
  {
    return TRUE;
  }


  public function store_settings($params)
  {
    $mod = cge_utils::get_module('CGTiny');
    $keys = array('yourls_url','yourls_signature');
    $res = FALSE;
    foreach( $keys as $key )
      {
	if( isset($params[$key]) )
	  {
	    $mod->SetPreference($key,$params[$key]);
	    $res = TRUE;
	  }
      }
    return $res;
  }


  public function get_description()
  {
    return $this->get_module()->Lang('yourls_description');
  }

  public function get_controls()
  {
    $this->authenticate();
    $mod = cge_utils::get_module('CGTiny');
    return array(
		 array($mod->Lang('yourls_url'),
		       $mod->CreateInputText('m1_','yourls_url',$this->_url,40,40)),
		 array($mod->Lang('yourls_signature'),
		       $mod->CreateInputText('m1_','yourls_signature',$this->_signature,40,40))
		 );
  }


  public function is_short( $url ) {
    $this->authenticate();
    return stristr( $url, $this->_url );
  }


  public function authenticate()
  {
    $mod = cge_utils::get_module('CGTiny');
    if( is_object($mod) )
      {
	$this->_url = $mod->GetPreference('yourls_url');
	$this->_signature = $mod->GetPreference('yourls_signature');
	return TRUE;
      }
    return FALSE;
  }


  public function shorten($url)
  {
    if( !$this->authenticate() ) return $url;

    $the_url = $this->_url;
    if( !endswith('/yourls-api.php',$the_url) )
      {
	$the_url .= '/yourls-api.php';
      }

    $data = array('action'=>'shorturl',
		  'format'=>'json',
		  'signature'=>$this->_signature,
		  'url'=>$url);
    $result = cge_http::post($the_url,$data);
    if( !$result ) return $url;
    $result = json_decode($result);
    if( !is_object($result) ) return $url;
    if( !isset($result->shorturl) ) return $url;
    if( isset($result->status) && $result->status == 'fail' )
      {
	// did it really fail.. or is it just faking us?
	if( isset($result->message) && strstr($result->message,'already exists') !== FALSE )
	  {
	    // not an error.
	  }
	else
	  {
	    return $url;
	  }
      }

    // todo: audit this.
    return $result->shorturl;
  }

  public function lengthen($url)
  {
    if( !$this->authenticate() ) return $url;

    $the_url = $this->_url;
    if( !endswith('/yourls-api.php',$the_url) )
      {
	$the_url .= '/yourls-api.php';
      }

    $data = array('format'=>'json',
                  'action'=>'expand',
		  'signature'=>$this->_signature,
		  'shorturl'=>$url);
    $result = cge_http::post($the_url,$data);
    if( !$result ) return $url;
    $result = json_decode($result);
    if( !$result->status )
      {
	foreach( $result->results as $detail )
	  {
	    return $detail->longUrl;
	  }
      }
    else
      {
	// todo: audit this.
	return $url;
      }
  }
}

#
# EOF
#
?>