<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20011 by Robert Campbell 
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
# 
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGURL_Service_Trim extends CGURL_Service
{
  private $_username;
  private $_password;

  public function get_name()
  {
    return 'tr.im';
  }


  public function has_settings()
  {
    return TRUE;
  }


  public function store_settings($params)
  {
    $mod = cge_utils::get_module('CGTiny');
    $keys = array('trim_username','trim_password');
    $res = FALSE;
    foreach( $keys as $key )
      {
	if( isset($params[$key]) )
	  {
	    $mod->SetPreference($key,$params[$key]);
	    $res = TRUE;
	  }
      }
    return $res;
  }


  public function get_controls()
  {
    $this->authenticate();
    $mod = cge_utils::get_module('CGTiny');
    return array(
		 array($mod->Lang('trim_username'),
		       $mod->CreateInputText('m1_','trim_username',$this->_username,40,40)),
		 array($mod->Lang('trim_password'),
		       $mod->CreateInputText('m1_','trim_password',$this->_password,40,40))
		 );
  }


  public function authenticate()
  {
    $mod = cge_utils::get_module('CGTiny');
    if( is_object($mod) )
      {
	$this->_username = $mod->GetPreference('trim_username');
	$this->_password = $mod->GetPreference('trim_password');
	return TRUE;
      }
    return FALSE;
  }


  public function is_short( $url ) {
    return !stristr( $url, 'tr.im' );
  }


  public function shorten($url)
  {  
    $this->authenticate();
    return cge_http::get( 'http://api.tr.im/api/trim_simple?url='.urlencode($url).'&username='.urlencode($this->_username).'&password='.urlencode($this->_password));
  }


  public function lengthen($url)
  {
    $bits = parse_url( $url );
    $result = $this->url_get( 'http://api.tr.im/api/trim_destionation.json?trimpath=' . substr( $bits['path'], 1 ) );
    $result = json_decode($result);
    if ( 'OK' == $result->status->result )
      return $result->destination;
    else
      return $url;
  }
}

#
# EOF
#
?>