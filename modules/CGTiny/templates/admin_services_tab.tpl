{literal}
<style type="text/css">
.scrollable {
  display: none;
  width: 90%;
}
#list ul {
  width: 90%;
}
#list ul li {
  padding-top: 0.5em;
  padding-bottom: 0.5em;
  border-bottom: 1px dashed grey;
}
.modified {
  display: none;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
  $('.edit_link').click(function(){
    var sel = $(this).attr('rel');
    $('.scrollable').hide('slide', { direction: 'left' }, 250 );
    $(':submit.save_services').hide('slow');
    $(sel).show('slide', { direction: 'right'}, 250 );
    return false;
  });
  $('.back_link').click(function(){
    $('.scrollable').hide('slide', { direction: 'right' }, 250 );
    $(':submit.save_services').show('slow');
    $('#list').show('slide', { direction: 'left' }, 250 );
    return false;
  });
  $('#service_list').tableDnD();
  $(':input').change(function(){
    var id = $(this).closest('div.scrollable').attr('id').substr(4);
    $('#'+id+' span.modified').show();
  });
  $(':submit.save_services').click(function(){
    var data = new Array;
    $('table#service_list tr').each(function(i,tr){
      data.push($(tr).attr('id'));
    });
    data = data.join(',');
    $('#serialdata').val(data);
  });
  $('.img').live('click',function(){
    var cn = $(this).closest('tr').attr('id');
    var str = ':hidden[name$="'+cn+'_active"]';
    var v = $(str).val();
    if( v == 0 ) v = 1; else v = 0;
    $(str).val(v);
    var s = '{/literal}{$img_yes}{literal}';
    if( !v )
     {
       s = '{/literal}{$img_no}{literal}';
     }
    $(this).html(s);
  });
});
</script>
{/literal}

{$formstart}
<input id="serialdata" type="hidden" name="{$actionid}serialdata" value=""/>
<div id="list" class="scrollable" style="display: block;">
<table id="service_list" class="pagetable" cellspacing="0">
  <thead>
   <tr>
     <th>{$mod->Lang('name')}</th>
     <th class="pageicon">{$mod->Lang('active')}</th>
     <th class="pageicon">{$mod->Lang('modified')}</th>
     <th class="pageicon"></th>
   </tr>
  </thead>
  <tbody>
  {foreach from=$entries item='item'}
    <tr id="{$item.classname}">
     <td>{if $item.obj->has_settings()}<a class="edit_link" rel="#sel_{$item.classname}">{$item.service}</a>{else}{$item.service}{/if}</td>
     <td><input type="hidden" name="{$actionid}{$item.classname}_active" value="{$item.active}"/><center><span class="img">{if $item.active}{$img_yes}{else}{$img_no}{/if}</span></center></td>
     <td>
      <span class="modified"><center>{$img_warn}</center></span>
     </td>
     <td>
      {if $item.obj->has_settings()}
      <a class="edit_link" rel="#sel_{$item.classname}"><img src="../modules/{$mod->GetName()}/icons/iphone_edit_rgt.png" alt="{$mod->Lang('edit')}" height="16"/></a>
      {/if}
     </td>
    </tr>
  {/foreach}
  </tbody>
</table>
</div>

{foreach from=$entries item='item'}
{if $item.obj->has_settings()}
  <div class="scrollable" id="sel_{$item.classname}">
    <div class="header"><a class="back_link"><img src="../modules/CGTiny/icons/iphone_back_lft.png" alt="{$mod->Lang('back')}"/></a></div>
    <fieldset>
      <legend>{$mod->Lang('settings_for')}: {$item.service}</legend>
      {$item.obj->get_description()}
      {foreach from=$item.obj->get_controls() item='control'}
        <div class="pageoverflow">
          <p class="pagetext">{$control[0]}</p>
          <p class="pageinput">{$control[1]}
           {if count($control) == 3}<br/>{$control[2]}{/if}
          </p>
        </div>
      {/foreach}
    </fieldset>
  </div>
{/if}
{/foreach}

<div class="pageoverflow">
  <p class="pagetext"></p>
  <p class="pageinput">
    <input class="save_services" type="submit" name="{$actionid}submit" value="{$mod->Lang('save')}"/>
  </p>
</div>
{$formend}