<?php
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

$db =& $this->GetDb();
$dict = NewDataDictionary($db);
$taboptarray = array('mysql' => 'TYPE=MyISAM');

// Site Tables
$flds = "
	site_id					I KEY,
	site_name 				C(255),
	site_url 				C(255),
	site_filedate			C(255),
	site_fileupdate			C(255),
	site_filepwd 			C(255),
	site_cms_version		C(25),
	site_admin_url			C(255),
	site_php_version		C(25),
	site_module_list		X,
	site_ssi_version		C(25),
	site_config_writable	C(25),
	site_installer_present	C(25),
	site_maintenance_mode	C(25)";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix().'module_simplesitemgr_sites', $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

$db->CreateSequence(cms_db_prefix()."module_simplesitemgr_sites_seq");

// Site Module Tables
$flds = "
	site_id			I,
	core			C(1),
	module_name 	C(100),
	module_version 	C(15)";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix().'module_simplesitemgr_site_modules', $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

?>